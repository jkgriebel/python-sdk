# coding: utf-8

"""
    3blades API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)

    OpenAPI spec version: 1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import sys
import os
import re

# python 2 and python 3 compatibility library
from six import iteritems

from ..configuration import Configuration
from ..api_client import ApiClient


class ServersApi(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        config = Configuration()
        if api_client:
            self.api_client = api_client
        else:
            if not config.api_client:
                config.api_client = ApiClient()
            self.api_client = config.api_client

    def servers_options_resources_read(self, size, **kwargs):
        """
        Get a server size by id
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.servers_options_resources_read(size, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str size: Server size unique identifier expressed as UUID or name. (required)
        :return: ServerSize
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.servers_options_resources_read_with_http_info(size, **kwargs)
        else:
            (data) = self.servers_options_resources_read_with_http_info(size, **kwargs)
            return data

    def servers_options_resources_read_with_http_info(self, size, **kwargs):
        """
        Get a server size by id
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.servers_options_resources_read_with_http_info(size, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str size: Server size unique identifier expressed as UUID or name. (required)
        :return: ServerSize
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['size']
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method servers_options_resources_read" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'size' is set
        if ('size' not in params) or (params['size'] is None):
            raise ValueError("Missing the required parameter `size` when calling `servers_options_resources_read`")


        collection_formats = {}

        path_params = {}
        if 'size' in params:
            path_params['size'] = params['size']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json', 'text/html'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = ['jwt']

        return self.api_client.call_api('/v1/servers/options/server-size/{size}/', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='ServerSize',
                                        auth_settings=auth_settings,
                                        callback=params.get('callback'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def servers_options_server_size_create(self, **kwargs):
        """
        Create a new server size item
        Only super users with on-premises version have acceess to this endpoint.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.servers_options_server_size_create(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param ServerSizeData serversize_data:
        :return: ServerSize
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.servers_options_server_size_create_with_http_info(**kwargs)
        else:
            (data) = self.servers_options_server_size_create_with_http_info(**kwargs)
            return data

    def servers_options_server_size_create_with_http_info(self, **kwargs):
        """
        Create a new server size item
        Only super users with on-premises version have acceess to this endpoint.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.servers_options_server_size_create_with_http_info(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param ServerSizeData serversize_data:
        :return: ServerSize
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['serversize_data']
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method servers_options_server_size_create" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'serversize_data' in params:
            body_params = params['serversize_data']
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json', 'text/html'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = ['jwt']

        return self.api_client.call_api('/v1/servers/options/server-size/', 'POST',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='ServerSize',
                                        auth_settings=auth_settings,
                                        callback=params.get('callback'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def servers_options_server_size_delete(self, size, **kwargs):
        """
        Delete a server size by id
        Only super users with on-premises version have acceess to this endpoint.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.servers_options_server_size_delete(size, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str size: Server size unique identifier expressed as UUID or name. (required)
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.servers_options_server_size_delete_with_http_info(size, **kwargs)
        else:
            (data) = self.servers_options_server_size_delete_with_http_info(size, **kwargs)
            return data

    def servers_options_server_size_delete_with_http_info(self, size, **kwargs):
        """
        Delete a server size by id
        Only super users with on-premises version have acceess to this endpoint.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.servers_options_server_size_delete_with_http_info(size, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str size: Server size unique identifier expressed as UUID or name. (required)
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['size']
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method servers_options_server_size_delete" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'size' is set
        if ('size' not in params) or (params['size'] is None):
            raise ValueError("Missing the required parameter `size` when calling `servers_options_server_size_delete`")


        collection_formats = {}

        path_params = {}
        if 'size' in params:
            path_params['size'] = params['size']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json', 'text/html'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = ['jwt']

        return self.api_client.call_api('/v1/servers/options/server-size/{size}/', 'DELETE',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type=None,
                                        auth_settings=auth_settings,
                                        callback=params.get('callback'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def servers_options_server_size_replace(self, size, **kwargs):
        """
        Replace a server size by id
        Only super users with on-premises version have acceess to this endpoint.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.servers_options_server_size_replace(size, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str size: Server size unique identifier expressed as UUID or name. (required)
        :param ServerSizeData serversize_data:
        :return: ServerSize
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.servers_options_server_size_replace_with_http_info(size, **kwargs)
        else:
            (data) = self.servers_options_server_size_replace_with_http_info(size, **kwargs)
            return data

    def servers_options_server_size_replace_with_http_info(self, size, **kwargs):
        """
        Replace a server size by id
        Only super users with on-premises version have acceess to this endpoint.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.servers_options_server_size_replace_with_http_info(size, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str size: Server size unique identifier expressed as UUID or name. (required)
        :param ServerSizeData serversize_data:
        :return: ServerSize
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['size', 'serversize_data']
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method servers_options_server_size_replace" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'size' is set
        if ('size' not in params) or (params['size'] is None):
            raise ValueError("Missing the required parameter `size` when calling `servers_options_server_size_replace`")


        collection_formats = {}

        path_params = {}
        if 'size' in params:
            path_params['size'] = params['size']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'serversize_data' in params:
            body_params = params['serversize_data']
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json', 'text/html'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = ['jwt']

        return self.api_client.call_api('/v1/servers/options/server-size/{size}/', 'PUT',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='ServerSize',
                                        auth_settings=auth_settings,
                                        callback=params.get('callback'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def servers_options_server_size_update(self, size, **kwargs):
        """
        Update a server size by id
        Only super users with on-premises version have acceess to this endpoint.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.servers_options_server_size_update(size, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str size: Server size unique identifier expressed as UUID or name. (required)
        :param ServerSizeData serversize_data:
        :return: ServerSize
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.servers_options_server_size_update_with_http_info(size, **kwargs)
        else:
            (data) = self.servers_options_server_size_update_with_http_info(size, **kwargs)
            return data

    def servers_options_server_size_update_with_http_info(self, size, **kwargs):
        """
        Update a server size by id
        Only super users with on-premises version have acceess to this endpoint.
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.servers_options_server_size_update_with_http_info(size, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str size: Server size unique identifier expressed as UUID or name. (required)
        :param ServerSizeData serversize_data:
        :return: ServerSize
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['size', 'serversize_data']
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method servers_options_server_size_update" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'size' is set
        if ('size' not in params) or (params['size'] is None):
            raise ValueError("Missing the required parameter `size` when calling `servers_options_server_size_update`")


        collection_formats = {}

        path_params = {}
        if 'size' in params:
            path_params['size'] = params['size']

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'serversize_data' in params:
            body_params = params['serversize_data']
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json', 'text/html'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = ['jwt']

        return self.api_client.call_api('/v1/servers/options/server-size/{size}/', 'PATCH',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='ServerSize',
                                        auth_settings=auth_settings,
                                        callback=params.get('callback'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)

    def servers_options_sizes_list(self, **kwargs):
        """
        Retrieve available server sizes
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.servers_options_sizes_list(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str limit: Set limit when retrieving items.
        :param str offset: Offset when retrieving items.
        :param str ordering: Set order when retrieving items.
        :return: list[ServerSize]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.servers_options_sizes_list_with_http_info(**kwargs)
        else:
            (data) = self.servers_options_sizes_list_with_http_info(**kwargs)
            return data

    def servers_options_sizes_list_with_http_info(self, **kwargs):
        """
        Retrieve available server sizes
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.servers_options_sizes_list_with_http_info(callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str limit: Set limit when retrieving items.
        :param str offset: Offset when retrieving items.
        :param str ordering: Set order when retrieving items.
        :return: list[ServerSize]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['limit', 'offset', 'ordering']
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method servers_options_sizes_list" % key
                )
            params[key] = val
        del params['kwargs']


        collection_formats = {}

        path_params = {}

        query_params = []
        if 'limit' in params:
            query_params.append(('limit', params['limit']))
        if 'offset' in params:
            query_params.append(('offset', params['offset']))
        if 'ordering' in params:
            query_params.append(('ordering', params['ordering']))

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json', 'text/html'])

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/json'])

        # Authentication setting
        auth_settings = ['jwt']

        return self.api_client.call_api('/v1/servers/options/server-size/', 'GET',
                                        path_params,
                                        query_params,
                                        header_params,
                                        body=body_params,
                                        post_params=form_params,
                                        files=local_var_files,
                                        response_type='list[ServerSize]',
                                        auth_settings=auth_settings,
                                        callback=params.get('callback'),
                                        _return_http_data_only=params.get('_return_http_data_only'),
                                        _preload_content=params.get('_preload_content', True),
                                        _request_timeout=params.get('_request_timeout'),
                                        collection_formats=collection_formats)
