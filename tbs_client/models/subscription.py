# coding: utf-8

"""
    3blades API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)

    OpenAPI spec version: 1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class Subscription(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'plan': 'str',
        'stripe_id': 'str',
        'created': 'str',
        'livemode': 'bool',
        'application_fee_percent': 'float',
        'cancel_at_period_end': 'bool',
        'canceled_at': 'str',
        'current_period_start': 'str',
        'current_period_end': 'str',
        'start': 'str',
        'ended_at': 'str',
        'quantity': 'int',
        'status': 'str',
        'trial_start': 'str',
        'trial_end': 'str'
    }

    attribute_map = {
        'id': 'id',
        'plan': 'plan',
        'stripe_id': 'stripe_id',
        'created': 'created',
        'livemode': 'livemode',
        'application_fee_percent': 'application_fee_percent',
        'cancel_at_period_end': 'cancel_at_period_end',
        'canceled_at': 'canceled_at',
        'current_period_start': 'current_period_start',
        'current_period_end': 'current_period_end',
        'start': 'start',
        'ended_at': 'ended_at',
        'quantity': 'quantity',
        'status': 'status',
        'trial_start': 'trial_start',
        'trial_end': 'trial_end'
    }

    def __init__(self, id=None, plan=None, stripe_id=None, created=None, livemode=None, application_fee_percent=None, cancel_at_period_end=None, canceled_at=None, current_period_start=None, current_period_end=None, start=None, ended_at=None, quantity=None, status=None, trial_start=None, trial_end=None):
        """
        Subscription - a model defined in Swagger
        """

        self._id = None
        self._plan = None
        self._stripe_id = None
        self._created = None
        self._livemode = None
        self._application_fee_percent = None
        self._cancel_at_period_end = None
        self._canceled_at = None
        self._current_period_start = None
        self._current_period_end = None
        self._start = None
        self._ended_at = None
        self._quantity = None
        self._status = None
        self._trial_start = None
        self._trial_end = None

        if id is not None:
          self.id = id
        self.plan = plan
        if stripe_id is not None:
          self.stripe_id = stripe_id
        if created is not None:
          self.created = created
        if livemode is not None:
          self.livemode = livemode
        if application_fee_percent is not None:
          self.application_fee_percent = application_fee_percent
        if cancel_at_period_end is not None:
          self.cancel_at_period_end = cancel_at_period_end
        if canceled_at is not None:
          self.canceled_at = canceled_at
        if current_period_start is not None:
          self.current_period_start = current_period_start
        if current_period_end is not None:
          self.current_period_end = current_period_end
        if start is not None:
          self.start = start
        if ended_at is not None:
          self.ended_at = ended_at
        if quantity is not None:
          self.quantity = quantity
        if status is not None:
          self.status = status
        if trial_start is not None:
          self.trial_start = trial_start
        if trial_end is not None:
          self.trial_end = trial_end

    @property
    def id(self):
        """
        Gets the id of this Subscription.
        Unique identifier for suscription as UUID.

        :return: The id of this Subscription.
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """
        Sets the id of this Subscription.
        Unique identifier for suscription as UUID.

        :param id: The id of this Subscription.
        :type: str
        """

        self._id = id

    @property
    def plan(self):
        """
        Gets the plan of this Subscription.
        Plan name.

        :return: The plan of this Subscription.
        :rtype: str
        """
        return self._plan

    @plan.setter
    def plan(self, plan):
        """
        Sets the plan of this Subscription.
        Plan name.

        :param plan: The plan of this Subscription.
        :type: str
        """
        if plan is None:
            raise ValueError("Invalid value for `plan`, must not be `None`")

        self._plan = plan

    @property
    def stripe_id(self):
        """
        Gets the stripe_id of this Subscription.
        Stripe (payment processor) identifier.

        :return: The stripe_id of this Subscription.
        :rtype: str
        """
        return self._stripe_id

    @stripe_id.setter
    def stripe_id(self, stripe_id):
        """
        Sets the stripe_id of this Subscription.
        Stripe (payment processor) identifier.

        :param stripe_id: The stripe_id of this Subscription.
        :type: str
        """

        self._stripe_id = stripe_id

    @property
    def created(self):
        """
        Gets the created of this Subscription.
        Date and time suscription was created.

        :return: The created of this Subscription.
        :rtype: str
        """
        return self._created

    @created.setter
    def created(self, created):
        """
        Sets the created of this Subscription.
        Date and time suscription was created.

        :param created: The created of this Subscription.
        :type: str
        """

        self._created = created

    @property
    def livemode(self):
        """
        Gets the livemode of this Subscription.
        Suscription live, or not.

        :return: The livemode of this Subscription.
        :rtype: bool
        """
        return self._livemode

    @livemode.setter
    def livemode(self, livemode):
        """
        Sets the livemode of this Subscription.
        Suscription live, or not.

        :param livemode: The livemode of this Subscription.
        :type: bool
        """

        self._livemode = livemode

    @property
    def application_fee_percent(self):
        """
        Gets the application_fee_percent of this Subscription.
        Application fee percent.

        :return: The application_fee_percent of this Subscription.
        :rtype: float
        """
        return self._application_fee_percent

    @application_fee_percent.setter
    def application_fee_percent(self, application_fee_percent):
        """
        Sets the application_fee_percent of this Subscription.
        Application fee percent.

        :param application_fee_percent: The application_fee_percent of this Subscription.
        :type: float
        """

        self._application_fee_percent = application_fee_percent

    @property
    def cancel_at_period_end(self):
        """
        Gets the cancel_at_period_end of this Subscription.
        Boolean value to determine whether plan cancels at the end of the period, or not.

        :return: The cancel_at_period_end of this Subscription.
        :rtype: bool
        """
        return self._cancel_at_period_end

    @cancel_at_period_end.setter
    def cancel_at_period_end(self, cancel_at_period_end):
        """
        Sets the cancel_at_period_end of this Subscription.
        Boolean value to determine whether plan cancels at the end of the period, or not.

        :param cancel_at_period_end: The cancel_at_period_end of this Subscription.
        :type: bool
        """

        self._cancel_at_period_end = cancel_at_period_end

    @property
    def canceled_at(self):
        """
        Gets the canceled_at of this Subscription.
        Date and time when plan was cancelled.

        :return: The canceled_at of this Subscription.
        :rtype: str
        """
        return self._canceled_at

    @canceled_at.setter
    def canceled_at(self, canceled_at):
        """
        Sets the canceled_at of this Subscription.
        Date and time when plan was cancelled.

        :param canceled_at: The canceled_at of this Subscription.
        :type: str
        """

        self._canceled_at = canceled_at

    @property
    def current_period_start(self):
        """
        Gets the current_period_start of this Subscription.
        Current suscription plan start date.

        :return: The current_period_start of this Subscription.
        :rtype: str
        """
        return self._current_period_start

    @current_period_start.setter
    def current_period_start(self, current_period_start):
        """
        Sets the current_period_start of this Subscription.
        Current suscription plan start date.

        :param current_period_start: The current_period_start of this Subscription.
        :type: str
        """

        self._current_period_start = current_period_start

    @property
    def current_period_end(self):
        """
        Gets the current_period_end of this Subscription.
        Current suscription plan end date.

        :return: The current_period_end of this Subscription.
        :rtype: str
        """
        return self._current_period_end

    @current_period_end.setter
    def current_period_end(self, current_period_end):
        """
        Sets the current_period_end of this Subscription.
        Current suscription plan end date.

        :param current_period_end: The current_period_end of this Subscription.
        :type: str
        """

        self._current_period_end = current_period_end

    @property
    def start(self):
        """
        Gets the start of this Subscription.
        Date and time for when plan started.

        :return: The start of this Subscription.
        :rtype: str
        """
        return self._start

    @start.setter
    def start(self, start):
        """
        Sets the start of this Subscription.
        Date and time for when plan started.

        :param start: The start of this Subscription.
        :type: str
        """

        self._start = start

    @property
    def ended_at(self):
        """
        Gets the ended_at of this Subscription.
        Date and time for when plan ended.

        :return: The ended_at of this Subscription.
        :rtype: str
        """
        return self._ended_at

    @ended_at.setter
    def ended_at(self, ended_at):
        """
        Sets the ended_at of this Subscription.
        Date and time for when plan ended.

        :param ended_at: The ended_at of this Subscription.
        :type: str
        """

        self._ended_at = ended_at

    @property
    def quantity(self):
        """
        Gets the quantity of this Subscription.
        Quantity purchased as integer.

        :return: The quantity of this Subscription.
        :rtype: int
        """
        return self._quantity

    @quantity.setter
    def quantity(self, quantity):
        """
        Sets the quantity of this Subscription.
        Quantity purchased as integer.

        :param quantity: The quantity of this Subscription.
        :type: int
        """

        self._quantity = quantity

    @property
    def status(self):
        """
        Gets the status of this Subscription.
        Suscription status.

        :return: The status of this Subscription.
        :rtype: str
        """
        return self._status

    @status.setter
    def status(self, status):
        """
        Sets the status of this Subscription.
        Suscription status.

        :param status: The status of this Subscription.
        :type: str
        """

        self._status = status

    @property
    def trial_start(self):
        """
        Gets the trial_start of this Subscription.
        Date and time for trial start.

        :return: The trial_start of this Subscription.
        :rtype: str
        """
        return self._trial_start

    @trial_start.setter
    def trial_start(self, trial_start):
        """
        Sets the trial_start of this Subscription.
        Date and time for trial start.

        :param trial_start: The trial_start of this Subscription.
        :type: str
        """

        self._trial_start = trial_start

    @property
    def trial_end(self):
        """
        Gets the trial_end of this Subscription.
        Date and time for trial end.

        :return: The trial_end of this Subscription.
        :rtype: str
        """
        return self._trial_end

    @trial_end.setter
    def trial_end(self, trial_end):
        """
        Sets the trial_end of this Subscription.
        Date and time for trial end.

        :param trial_end: The trial_end of this Subscription.
        :type: str
        """

        self._trial_end = trial_end

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, Subscription):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
