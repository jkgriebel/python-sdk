# coding: utf-8

"""
    3blades API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)

    OpenAPI spec version: 1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class Invoice(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'stripe_id': 'str',
        'created': 'str',
        'metadata': 'object',
        'livemode': 'bool',
        'amount_due': 'int',
        'application_fee': 'int',
        'attempt_count': 'int',
        'attempted': 'bool',
        'closed': 'bool',
        'currency': 'str',
        'invoice_date': 'str',
        'description': 'str',
        'next_payment_attempt': 'str',
        'paid': 'bool',
        'period_start': 'str',
        'period_end': 'str',
        'reciept_number': 'str',
        'starting_balance': 'int',
        'statement_descriptor': 'str',
        'subtotal': 'int',
        'tax': 'int',
        'total': 'int',
        'customer': 'str',
        'subscription': 'str'
    }

    attribute_map = {
        'id': 'id',
        'stripe_id': 'stripe_id',
        'created': 'created',
        'metadata': 'metadata',
        'livemode': 'livemode',
        'amount_due': 'amount_due',
        'application_fee': 'application_fee',
        'attempt_count': 'attempt_count',
        'attempted': 'attempted',
        'closed': 'closed',
        'currency': 'currency',
        'invoice_date': 'invoice_date',
        'description': 'description',
        'next_payment_attempt': 'next_payment_attempt',
        'paid': 'paid',
        'period_start': 'period_start',
        'period_end': 'period_end',
        'reciept_number': 'reciept_number',
        'starting_balance': 'starting_balance',
        'statement_descriptor': 'statement_descriptor',
        'subtotal': 'subtotal',
        'tax': 'tax',
        'total': 'total',
        'customer': 'customer',
        'subscription': 'subscription'
    }

    def __init__(self, id=None, stripe_id=None, created=None, metadata=None, livemode=None, amount_due=None, application_fee=None, attempt_count=None, attempted=None, closed=None, currency=None, invoice_date=None, description=None, next_payment_attempt=None, paid=None, period_start=None, period_end=None, reciept_number=None, starting_balance=None, statement_descriptor=None, subtotal=None, tax=None, total=None, customer=None, subscription=None):
        """
        Invoice - a model defined in Swagger
        """

        self._id = None
        self._stripe_id = None
        self._created = None
        self._metadata = None
        self._livemode = None
        self._amount_due = None
        self._application_fee = None
        self._attempt_count = None
        self._attempted = None
        self._closed = None
        self._currency = None
        self._invoice_date = None
        self._description = None
        self._next_payment_attempt = None
        self._paid = None
        self._period_start = None
        self._period_end = None
        self._reciept_number = None
        self._starting_balance = None
        self._statement_descriptor = None
        self._subtotal = None
        self._tax = None
        self._total = None
        self._customer = None
        self._subscription = None

        if id is not None:
          self.id = id
        self.stripe_id = stripe_id
        self.created = created
        if metadata is not None:
          self.metadata = metadata
        if livemode is not None:
          self.livemode = livemode
        self.amount_due = amount_due
        if application_fee is not None:
          self.application_fee = application_fee
        if attempt_count is not None:
          self.attempt_count = attempt_count
        if attempted is not None:
          self.attempted = attempted
        if closed is not None:
          self.closed = closed
        self.currency = currency
        self.invoice_date = invoice_date
        if description is not None:
          self.description = description
        if next_payment_attempt is not None:
          self.next_payment_attempt = next_payment_attempt
        if paid is not None:
          self.paid = paid
        self.period_start = period_start
        self.period_end = period_end
        self.reciept_number = reciept_number
        self.starting_balance = starting_balance
        if statement_descriptor is not None:
          self.statement_descriptor = statement_descriptor
        self.subtotal = subtotal
        if tax is not None:
          self.tax = tax
        self.total = total
        self.customer = customer
        if subscription is not None:
          self.subscription = subscription

    @property
    def id(self):
        """
        Gets the id of this Invoice.
        Invoice unique identifier expressed as UUID.

        :return: The id of this Invoice.
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """
        Sets the id of this Invoice.
        Invoice unique identifier expressed as UUID.

        :param id: The id of this Invoice.
        :type: str
        """

        self._id = id

    @property
    def stripe_id(self):
        """
        Gets the stripe_id of this Invoice.
        Stripe account identifier.

        :return: The stripe_id of this Invoice.
        :rtype: str
        """
        return self._stripe_id

    @stripe_id.setter
    def stripe_id(self, stripe_id):
        """
        Sets the stripe_id of this Invoice.
        Stripe account identifier.

        :param stripe_id: The stripe_id of this Invoice.
        :type: str
        """
        if stripe_id is None:
            raise ValueError("Invalid value for `stripe_id`, must not be `None`")

        self._stripe_id = stripe_id

    @property
    def created(self):
        """
        Gets the created of this Invoice.
        Date and time when invoice was created.

        :return: The created of this Invoice.
        :rtype: str
        """
        return self._created

    @created.setter
    def created(self, created):
        """
        Sets the created of this Invoice.
        Date and time when invoice was created.

        :param created: The created of this Invoice.
        :type: str
        """
        if created is None:
            raise ValueError("Invalid value for `created`, must not be `None`")

        self._created = created

    @property
    def metadata(self):
        """
        Gets the metadata of this Invoice.
        Optional metadata object of invoice.

        :return: The metadata of this Invoice.
        :rtype: object
        """
        return self._metadata

    @metadata.setter
    def metadata(self, metadata):
        """
        Sets the metadata of this Invoice.
        Optional metadata object of invoice.

        :param metadata: The metadata of this Invoice.
        :type: object
        """

        self._metadata = metadata

    @property
    def livemode(self):
        """
        Gets the livemode of this Invoice.
        Boolean that determines whether invoice is live, or not.

        :return: The livemode of this Invoice.
        :rtype: bool
        """
        return self._livemode

    @livemode.setter
    def livemode(self, livemode):
        """
        Sets the livemode of this Invoice.
        Boolean that determines whether invoice is live, or not.

        :param livemode: The livemode of this Invoice.
        :type: bool
        """

        self._livemode = livemode

    @property
    def amount_due(self):
        """
        Gets the amount_due of this Invoice.
        Amount due set in invoice.

        :return: The amount_due of this Invoice.
        :rtype: int
        """
        return self._amount_due

    @amount_due.setter
    def amount_due(self, amount_due):
        """
        Sets the amount_due of this Invoice.
        Amount due set in invoice.

        :param amount_due: The amount_due of this Invoice.
        :type: int
        """
        if amount_due is None:
            raise ValueError("Invalid value for `amount_due`, must not be `None`")

        self._amount_due = amount_due

    @property
    def application_fee(self):
        """
        Gets the application_fee of this Invoice.
        Application fee set in invoice.

        :return: The application_fee of this Invoice.
        :rtype: int
        """
        return self._application_fee

    @application_fee.setter
    def application_fee(self, application_fee):
        """
        Sets the application_fee of this Invoice.
        Application fee set in invoice.

        :param application_fee: The application_fee of this Invoice.
        :type: int
        """

        self._application_fee = application_fee

    @property
    def attempt_count(self):
        """
        Gets the attempt_count of this Invoice.
        Number of attempts to deliver invoice.

        :return: The attempt_count of this Invoice.
        :rtype: int
        """
        return self._attempt_count

    @attempt_count.setter
    def attempt_count(self, attempt_count):
        """
        Sets the attempt_count of this Invoice.
        Number of attempts to deliver invoice.

        :param attempt_count: The attempt_count of this Invoice.
        :type: int
        """

        self._attempt_count = attempt_count

    @property
    def attempted(self):
        """
        Gets the attempted of this Invoice.
        Boolean to determine whether delivery attempt executed, or not.

        :return: The attempted of this Invoice.
        :rtype: bool
        """
        return self._attempted

    @attempted.setter
    def attempted(self, attempted):
        """
        Sets the attempted of this Invoice.
        Boolean to determine whether delivery attempt executed, or not.

        :param attempted: The attempted of this Invoice.
        :type: bool
        """

        self._attempted = attempted

    @property
    def closed(self):
        """
        Gets the closed of this Invoice.
        Invoice closed, or pending.

        :return: The closed of this Invoice.
        :rtype: bool
        """
        return self._closed

    @closed.setter
    def closed(self, closed):
        """
        Sets the closed of this Invoice.
        Invoice closed, or pending.

        :param closed: The closed of this Invoice.
        :type: bool
        """

        self._closed = closed

    @property
    def currency(self):
        """
        Gets the currency of this Invoice.
        Currency used in invoice.

        :return: The currency of this Invoice.
        :rtype: str
        """
        return self._currency

    @currency.setter
    def currency(self, currency):
        """
        Sets the currency of this Invoice.
        Currency used in invoice.

        :param currency: The currency of this Invoice.
        :type: str
        """
        if currency is None:
            raise ValueError("Invalid value for `currency`, must not be `None`")

        self._currency = currency

    @property
    def invoice_date(self):
        """
        Gets the invoice_date of this Invoice.
        Invoice issue date.

        :return: The invoice_date of this Invoice.
        :rtype: str
        """
        return self._invoice_date

    @invoice_date.setter
    def invoice_date(self, invoice_date):
        """
        Sets the invoice_date of this Invoice.
        Invoice issue date.

        :param invoice_date: The invoice_date of this Invoice.
        :type: str
        """
        if invoice_date is None:
            raise ValueError("Invalid value for `invoice_date`, must not be `None`")

        self._invoice_date = invoice_date

    @property
    def description(self):
        """
        Gets the description of this Invoice.
        Invoice description.

        :return: The description of this Invoice.
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """
        Sets the description of this Invoice.
        Invoice description.

        :param description: The description of this Invoice.
        :type: str
        """

        self._description = description

    @property
    def next_payment_attempt(self):
        """
        Gets the next_payment_attempt of this Invoice.
        Next payment attempt.

        :return: The next_payment_attempt of this Invoice.
        :rtype: str
        """
        return self._next_payment_attempt

    @next_payment_attempt.setter
    def next_payment_attempt(self, next_payment_attempt):
        """
        Sets the next_payment_attempt of this Invoice.
        Next payment attempt.

        :param next_payment_attempt: The next_payment_attempt of this Invoice.
        :type: str
        """

        self._next_payment_attempt = next_payment_attempt

    @property
    def paid(self):
        """
        Gets the paid of this Invoice.
        Determines whether invoice has been paid, or not.

        :return: The paid of this Invoice.
        :rtype: bool
        """
        return self._paid

    @paid.setter
    def paid(self, paid):
        """
        Sets the paid of this Invoice.
        Determines whether invoice has been paid, or not.

        :param paid: The paid of this Invoice.
        :type: bool
        """

        self._paid = paid

    @property
    def period_start(self):
        """
        Gets the period_start of this Invoice.
        Invoice start period.

        :return: The period_start of this Invoice.
        :rtype: str
        """
        return self._period_start

    @period_start.setter
    def period_start(self, period_start):
        """
        Sets the period_start of this Invoice.
        Invoice start period.

        :param period_start: The period_start of this Invoice.
        :type: str
        """
        if period_start is None:
            raise ValueError("Invalid value for `period_start`, must not be `None`")

        self._period_start = period_start

    @property
    def period_end(self):
        """
        Gets the period_end of this Invoice.
        Invoice end period.

        :return: The period_end of this Invoice.
        :rtype: str
        """
        return self._period_end

    @period_end.setter
    def period_end(self, period_end):
        """
        Sets the period_end of this Invoice.
        Invoice end period.

        :param period_end: The period_end of this Invoice.
        :type: str
        """
        if period_end is None:
            raise ValueError("Invalid value for `period_end`, must not be `None`")

        self._period_end = period_end

    @property
    def reciept_number(self):
        """
        Gets the reciept_number of this Invoice.
        Invoice receipt number.

        :return: The reciept_number of this Invoice.
        :rtype: str
        """
        return self._reciept_number

    @reciept_number.setter
    def reciept_number(self, reciept_number):
        """
        Sets the reciept_number of this Invoice.
        Invoice receipt number.

        :param reciept_number: The reciept_number of this Invoice.
        :type: str
        """
        if reciept_number is None:
            raise ValueError("Invalid value for `reciept_number`, must not be `None`")

        self._reciept_number = reciept_number

    @property
    def starting_balance(self):
        """
        Gets the starting_balance of this Invoice.
        Invoice starting balance.

        :return: The starting_balance of this Invoice.
        :rtype: int
        """
        return self._starting_balance

    @starting_balance.setter
    def starting_balance(self, starting_balance):
        """
        Sets the starting_balance of this Invoice.
        Invoice starting balance.

        :param starting_balance: The starting_balance of this Invoice.
        :type: int
        """
        if starting_balance is None:
            raise ValueError("Invalid value for `starting_balance`, must not be `None`")

        self._starting_balance = starting_balance

    @property
    def statement_descriptor(self):
        """
        Gets the statement_descriptor of this Invoice.
        Invoice statement descriptor.

        :return: The statement_descriptor of this Invoice.
        :rtype: str
        """
        return self._statement_descriptor

    @statement_descriptor.setter
    def statement_descriptor(self, statement_descriptor):
        """
        Sets the statement_descriptor of this Invoice.
        Invoice statement descriptor.

        :param statement_descriptor: The statement_descriptor of this Invoice.
        :type: str
        """

        self._statement_descriptor = statement_descriptor

    @property
    def subtotal(self):
        """
        Gets the subtotal of this Invoice.
        Invoice sub total.

        :return: The subtotal of this Invoice.
        :rtype: int
        """
        return self._subtotal

    @subtotal.setter
    def subtotal(self, subtotal):
        """
        Sets the subtotal of this Invoice.
        Invoice sub total.

        :param subtotal: The subtotal of this Invoice.
        :type: int
        """
        if subtotal is None:
            raise ValueError("Invalid value for `subtotal`, must not be `None`")

        self._subtotal = subtotal

    @property
    def tax(self):
        """
        Gets the tax of this Invoice.
        Tax, if applicable.

        :return: The tax of this Invoice.
        :rtype: int
        """
        return self._tax

    @tax.setter
    def tax(self, tax):
        """
        Sets the tax of this Invoice.
        Tax, if applicable.

        :param tax: The tax of this Invoice.
        :type: int
        """

        self._tax = tax

    @property
    def total(self):
        """
        Gets the total of this Invoice.
        Invoice total.

        :return: The total of this Invoice.
        :rtype: int
        """
        return self._total

    @total.setter
    def total(self, total):
        """
        Sets the total of this Invoice.
        Invoice total.

        :param total: The total of this Invoice.
        :type: int
        """
        if total is None:
            raise ValueError("Invalid value for `total`, must not be `None`")

        self._total = total

    @property
    def customer(self):
        """
        Gets the customer of this Invoice.
        Customer name.

        :return: The customer of this Invoice.
        :rtype: str
        """
        return self._customer

    @customer.setter
    def customer(self, customer):
        """
        Sets the customer of this Invoice.
        Customer name.

        :param customer: The customer of this Invoice.
        :type: str
        """
        if customer is None:
            raise ValueError("Invalid value for `customer`, must not be `None`")

        self._customer = customer

    @property
    def subscription(self):
        """
        Gets the subscription of this Invoice.
        Suscription name.

        :return: The subscription of this Invoice.
        :rtype: str
        """
        return self._subscription

    @subscription.setter
    def subscription(self, subscription):
        """
        Sets the subscription of this Invoice.
        Suscription name.

        :param subscription: The subscription of this Invoice.
        :type: str
        """

        self._subscription = subscription

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, Invoice):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
