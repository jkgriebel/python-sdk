# coding: utf-8

"""
    3blades API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)

    OpenAPI spec version: 1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class ProjectFile(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'project': 'str',
        'content': 'str',
        'name': 'str',
        'path': 'str'
    }

    attribute_map = {
        'id': 'id',
        'project': 'project',
        'content': 'content',
        'name': 'name',
        'path': 'path'
    }

    def __init__(self, id=None, project=None, content=None, name=None, path=None):
        """
        ProjectFile - a model defined in Swagger
        """

        self._id = None
        self._project = None
        self._content = None
        self._name = None
        self._path = None

        if id is not None:
          self.id = id
        self.project = project
        if content is not None:
          self.content = content
        if name is not None:
          self.name = name
        if path is not None:
          self.path = path

    @property
    def id(self):
        """
        Gets the id of this ProjectFile.
        File unique identifier in UUID format.

        :return: The id of this ProjectFile.
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """
        Sets the id of this ProjectFile.
        File unique identifier in UUID format.

        :param id: The id of this ProjectFile.
        :type: str
        """

        self._id = id

    @property
    def project(self):
        """
        Gets the project of this ProjectFile.
        Project name where file is located.

        :return: The project of this ProjectFile.
        :rtype: str
        """
        return self._project

    @project.setter
    def project(self, project):
        """
        Sets the project of this ProjectFile.
        Project name where file is located.

        :param project: The project of this ProjectFile.
        :type: str
        """
        if project is None:
            raise ValueError("Invalid value for `project`, must not be `None`")

        self._project = project

    @property
    def content(self):
        """
        Gets the content of this ProjectFile.
        Data sent as string, in base64 format.

        :return: The content of this ProjectFile.
        :rtype: str
        """
        return self._content

    @content.setter
    def content(self, content):
        """
        Sets the content of this ProjectFile.
        Data sent as string, in base64 format.

        :param content: The content of this ProjectFile.
        :type: str
        """

        self._content = content

    @property
    def name(self):
        """
        Gets the name of this ProjectFile.
        File name and extension.

        :return: The name of this ProjectFile.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """
        Sets the name of this ProjectFile.
        File name and extension.

        :param name: The name of this ProjectFile.
        :type: str
        """

        self._name = name

    @property
    def path(self):
        """
        Gets the path of this ProjectFile.
        File path. Defaults to root (/).

        :return: The path of this ProjectFile.
        :rtype: str
        """
        return self._path

    @path.setter
    def path(self, path):
        """
        Sets the path of this ProjectFile.
        File path. Defaults to root (/).

        :param path: The path of this ProjectFile.
        :type: str
        """

        self._path = path

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, ProjectFile):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
