# coding: utf-8

"""
    3blades API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)

    OpenAPI spec version: 1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class InvoiceItem(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'stripe_id': 'str',
        'created': 'str',
        'metadata': 'object',
        'livemode': 'bool',
        'invoice': 'str',
        'amount': 'int',
        'currency': 'str',
        'invoice_date': 'str',
        'proration': 'bool',
        'quantity': 'int',
        'description': 'str'
    }

    attribute_map = {
        'id': 'id',
        'stripe_id': 'stripe_id',
        'created': 'created',
        'metadata': 'metadata',
        'livemode': 'livemode',
        'invoice': 'invoice',
        'amount': 'amount',
        'currency': 'currency',
        'invoice_date': 'invoice_date',
        'proration': 'proration',
        'quantity': 'quantity',
        'description': 'description'
    }

    def __init__(self, id=None, stripe_id=None, created=None, metadata=None, livemode=None, invoice=None, amount=None, currency=None, invoice_date=None, proration=None, quantity=None, description=None):
        """
        InvoiceItem - a model defined in Swagger
        """

        self._id = None
        self._stripe_id = None
        self._created = None
        self._metadata = None
        self._livemode = None
        self._invoice = None
        self._amount = None
        self._currency = None
        self._invoice_date = None
        self._proration = None
        self._quantity = None
        self._description = None

        if id is not None:
          self.id = id
        self.stripe_id = stripe_id
        self.created = created
        if metadata is not None:
          self.metadata = metadata
        if livemode is not None:
          self.livemode = livemode
        self.invoice = invoice
        self.amount = amount
        self.currency = currency
        self.invoice_date = invoice_date
        if proration is not None:
          self.proration = proration
        self.quantity = quantity
        if description is not None:
          self.description = description

    @property
    def id(self):
        """
        Gets the id of this InvoiceItem.
        InvoiceItem unique identifier expressed as UUID.

        :return: The id of this InvoiceItem.
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """
        Sets the id of this InvoiceItem.
        InvoiceItem unique identifier expressed as UUID.

        :param id: The id of this InvoiceItem.
        :type: str
        """

        self._id = id

    @property
    def stripe_id(self):
        """
        Gets the stripe_id of this InvoiceItem.
        Stripe account identifier.

        :return: The stripe_id of this InvoiceItem.
        :rtype: str
        """
        return self._stripe_id

    @stripe_id.setter
    def stripe_id(self, stripe_id):
        """
        Sets the stripe_id of this InvoiceItem.
        Stripe account identifier.

        :param stripe_id: The stripe_id of this InvoiceItem.
        :type: str
        """
        if stripe_id is None:
            raise ValueError("Invalid value for `stripe_id`, must not be `None`")

        self._stripe_id = stripe_id

    @property
    def created(self):
        """
        Gets the created of this InvoiceItem.
        Date and time when invoice was created.

        :return: The created of this InvoiceItem.
        :rtype: str
        """
        return self._created

    @created.setter
    def created(self, created):
        """
        Sets the created of this InvoiceItem.
        Date and time when invoice was created.

        :param created: The created of this InvoiceItem.
        :type: str
        """
        if created is None:
            raise ValueError("Invalid value for `created`, must not be `None`")

        self._created = created

    @property
    def metadata(self):
        """
        Gets the metadata of this InvoiceItem.
        Optional metadata object of invoice.

        :return: The metadata of this InvoiceItem.
        :rtype: object
        """
        return self._metadata

    @metadata.setter
    def metadata(self, metadata):
        """
        Sets the metadata of this InvoiceItem.
        Optional metadata object of invoice.

        :param metadata: The metadata of this InvoiceItem.
        :type: object
        """

        self._metadata = metadata

    @property
    def livemode(self):
        """
        Gets the livemode of this InvoiceItem.
        Boolean that determines whether invoice is live, or not.

        :return: The livemode of this InvoiceItem.
        :rtype: bool
        """
        return self._livemode

    @livemode.setter
    def livemode(self, livemode):
        """
        Sets the livemode of this InvoiceItem.
        Boolean that determines whether invoice is live, or not.

        :param livemode: The livemode of this InvoiceItem.
        :type: bool
        """

        self._livemode = livemode

    @property
    def invoice(self):
        """
        Gets the invoice of this InvoiceItem.
        Invoice unique identifier expressed as UUID.

        :return: The invoice of this InvoiceItem.
        :rtype: str
        """
        return self._invoice

    @invoice.setter
    def invoice(self, invoice):
        """
        Sets the invoice of this InvoiceItem.
        Invoice unique identifier expressed as UUID.

        :param invoice: The invoice of this InvoiceItem.
        :type: str
        """
        if invoice is None:
            raise ValueError("Invalid value for `invoice`, must not be `None`")

        self._invoice = invoice

    @property
    def amount(self):
        """
        Gets the amount of this InvoiceItem.
        Amount the the invoice item will be billed for.

        :return: The amount of this InvoiceItem.
        :rtype: int
        """
        return self._amount

    @amount.setter
    def amount(self, amount):
        """
        Sets the amount of this InvoiceItem.
        Amount the the invoice item will be billed for.

        :param amount: The amount of this InvoiceItem.
        :type: int
        """
        if amount is None:
            raise ValueError("Invalid value for `amount`, must not be `None`")

        self._amount = amount

    @property
    def currency(self):
        """
        Gets the currency of this InvoiceItem.
        Currency used in invoice.

        :return: The currency of this InvoiceItem.
        :rtype: str
        """
        return self._currency

    @currency.setter
    def currency(self, currency):
        """
        Sets the currency of this InvoiceItem.
        Currency used in invoice.

        :param currency: The currency of this InvoiceItem.
        :type: str
        """
        if currency is None:
            raise ValueError("Invalid value for `currency`, must not be `None`")

        self._currency = currency

    @property
    def invoice_date(self):
        """
        Gets the invoice_date of this InvoiceItem.
        Date the item was added to the invoice.

        :return: The invoice_date of this InvoiceItem.
        :rtype: str
        """
        return self._invoice_date

    @invoice_date.setter
    def invoice_date(self, invoice_date):
        """
        Sets the invoice_date of this InvoiceItem.
        Date the item was added to the invoice.

        :param invoice_date: The invoice_date of this InvoiceItem.
        :type: str
        """
        if invoice_date is None:
            raise ValueError("Invalid value for `invoice_date`, must not be `None`")

        self._invoice_date = invoice_date

    @property
    def proration(self):
        """
        Gets the proration of this InvoiceItem.
        Whether or not the items cost will be prorated for the billing period.

        :return: The proration of this InvoiceItem.
        :rtype: bool
        """
        return self._proration

    @proration.setter
    def proration(self, proration):
        """
        Sets the proration of this InvoiceItem.
        Whether or not the items cost will be prorated for the billing period.

        :param proration: The proration of this InvoiceItem.
        :type: bool
        """

        self._proration = proration

    @property
    def quantity(self):
        """
        Gets the quantity of this InvoiceItem.
        Number of units for this item.

        :return: The quantity of this InvoiceItem.
        :rtype: int
        """
        return self._quantity

    @quantity.setter
    def quantity(self, quantity):
        """
        Sets the quantity of this InvoiceItem.
        Number of units for this item.

        :param quantity: The quantity of this InvoiceItem.
        :type: int
        """
        if quantity is None:
            raise ValueError("Invalid value for `quantity`, must not be `None`")

        self._quantity = quantity

    @property
    def description(self):
        """
        Gets the description of this InvoiceItem.
        Item description.

        :return: The description of this InvoiceItem.
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """
        Sets the description of this InvoiceItem.
        Item description.

        :param description: The description of this InvoiceItem.
        :type: str
        """

        self._description = description

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, InvoiceItem):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
