# coding: utf-8

"""
    3blades API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)

    OpenAPI spec version: 1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class JWTError(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'non_field_errors': 'list[str]',
        'username': 'list[str]',
        'password': 'list[str]',
        'token': 'list[str]'
    }

    attribute_map = {
        'non_field_errors': 'non_field_errors',
        'username': 'username',
        'password': 'password',
        'token': 'token'
    }

    def __init__(self, non_field_errors=None, username=None, password=None, token=None):
        """
        JWTError - a model defined in Swagger
        """

        self._non_field_errors = None
        self._username = None
        self._password = None
        self._token = None

        if non_field_errors is not None:
          self.non_field_errors = non_field_errors
        if username is not None:
          self.username = username
        if password is not None:
          self.password = password
        if token is not None:
          self.token = token

    @property
    def non_field_errors(self):
        """
        Gets the non_field_errors of this JWTError.
        Errors not connected to any field

        :return: The non_field_errors of this JWTError.
        :rtype: list[str]
        """
        return self._non_field_errors

    @non_field_errors.setter
    def non_field_errors(self, non_field_errors):
        """
        Sets the non_field_errors of this JWTError.
        Errors not connected to any field

        :param non_field_errors: The non_field_errors of this JWTError.
        :type: list[str]
        """

        self._non_field_errors = non_field_errors

    @property
    def username(self):
        """
        Gets the username of this JWTError.
        username field errors

        :return: The username of this JWTError.
        :rtype: list[str]
        """
        return self._username

    @username.setter
    def username(self, username):
        """
        Sets the username of this JWTError.
        username field errors

        :param username: The username of this JWTError.
        :type: list[str]
        """

        self._username = username

    @property
    def password(self):
        """
        Gets the password of this JWTError.
        password field errors

        :return: The password of this JWTError.
        :rtype: list[str]
        """
        return self._password

    @password.setter
    def password(self, password):
        """
        Sets the password of this JWTError.
        password field errors

        :param password: The password of this JWTError.
        :type: list[str]
        """

        self._password = password

    @property
    def token(self):
        """
        Gets the token of this JWTError.
        token field errors

        :return: The token of this JWTError.
        :rtype: list[str]
        """
        return self._token

    @token.setter
    def token(self, token):
        """
        Sets the token of this JWTError.
        token field errors

        :param token: The token of this JWTError.
        :type: list[str]
        """

        self._token = token

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, JWTError):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
