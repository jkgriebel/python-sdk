# coding: utf-8

"""
    3blades API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)

    OpenAPI spec version: 1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class EmailData(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'address': 'str',
        'public': 'bool',
        'unsubscribed': 'bool'
    }

    attribute_map = {
        'address': 'address',
        'public': 'public',
        'unsubscribed': 'unsubscribed'
    }

    def __init__(self, address=None, public=None, unsubscribed=None):
        """
        EmailData - a model defined in Swagger
        """

        self._address = None
        self._public = None
        self._unsubscribed = None

        self.address = address
        if public is not None:
          self.public = public
        if unsubscribed is not None:
          self.unsubscribed = unsubscribed

    @property
    def address(self):
        """
        Gets the address of this EmailData.
        Email address.

        :return: The address of this EmailData.
        :rtype: str
        """
        return self._address

    @address.setter
    def address(self, address):
        """
        Sets the address of this EmailData.
        Email address.

        :param address: The address of this EmailData.
        :type: str
        """
        if address is None:
            raise ValueError("Invalid value for `address`, must not be `None`")

        self._address = address

    @property
    def public(self):
        """
        Gets the public of this EmailData.
        Public or private email address.

        :return: The public of this EmailData.
        :rtype: bool
        """
        return self._public

    @public.setter
    def public(self, public):
        """
        Sets the public of this EmailData.
        Public or private email address.

        :param public: The public of this EmailData.
        :type: bool
        """

        self._public = public

    @property
    def unsubscribed(self):
        """
        Gets the unsubscribed of this EmailData.
        Unsubscribed or suscribed.

        :return: The unsubscribed of this EmailData.
        :rtype: bool
        """
        return self._unsubscribed

    @unsubscribed.setter
    def unsubscribed(self, unsubscribed):
        """
        Sets the unsubscribed of this EmailData.
        Unsubscribed or suscribed.

        :param unsubscribed: The unsubscribed of this EmailData.
        :type: bool
        """

        self._unsubscribed = unsubscribed

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, EmailData):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
