# coding: utf-8

"""
    3blades API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)

    OpenAPI spec version: 1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class PlanData(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'metadata': 'object',
        'livemode': 'bool',
        'amount': 'int',
        'currency': 'str',
        'interval': 'list[str]',
        'interval_count': 'int',
        'name': 'str',
        'statement_descriptor': 'str',
        'trial_period_days': 'int'
    }

    attribute_map = {
        'metadata': 'metadata',
        'livemode': 'livemode',
        'amount': 'amount',
        'currency': 'currency',
        'interval': 'interval',
        'interval_count': 'interval_count',
        'name': 'name',
        'statement_descriptor': 'statement_descriptor',
        'trial_period_days': 'trial_period_days'
    }

    def __init__(self, metadata=None, livemode=None, amount=None, currency=None, interval=None, interval_count=None, name=None, statement_descriptor=None, trial_period_days=None):
        """
        PlanData - a model defined in Swagger
        """

        self._metadata = None
        self._livemode = None
        self._amount = None
        self._currency = None
        self._interval = None
        self._interval_count = None
        self._name = None
        self._statement_descriptor = None
        self._trial_period_days = None

        if metadata is not None:
          self.metadata = metadata
        if livemode is not None:
          self.livemode = livemode
        self.amount = amount
        if currency is not None:
          self.currency = currency
        self.interval = interval
        self.interval_count = interval_count
        self.name = name
        if statement_descriptor is not None:
          self.statement_descriptor = statement_descriptor
        if trial_period_days is not None:
          self.trial_period_days = trial_period_days

    @property
    def metadata(self):
        """
        Gets the metadata of this PlanData.
        Plan meta data.

        :return: The metadata of this PlanData.
        :rtype: object
        """
        return self._metadata

    @metadata.setter
    def metadata(self, metadata):
        """
        Sets the metadata of this PlanData.
        Plan meta data.

        :param metadata: The metadata of this PlanData.
        :type: object
        """

        self._metadata = metadata

    @property
    def livemode(self):
        """
        Gets the livemode of this PlanData.
        Is plan live, or not.

        :return: The livemode of this PlanData.
        :rtype: bool
        """
        return self._livemode

    @livemode.setter
    def livemode(self, livemode):
        """
        Sets the livemode of this PlanData.
        Is plan live, or not.

        :param livemode: The livemode of this PlanData.
        :type: bool
        """

        self._livemode = livemode

    @property
    def amount(self):
        """
        Gets the amount of this PlanData.
        Plan amount in currency.

        :return: The amount of this PlanData.
        :rtype: int
        """
        return self._amount

    @amount.setter
    def amount(self, amount):
        """
        Sets the amount of this PlanData.
        Plan amount in currency.

        :param amount: The amount of this PlanData.
        :type: int
        """
        if amount is None:
            raise ValueError("Invalid value for `amount`, must not be `None`")

        self._amount = amount

    @property
    def currency(self):
        """
        Gets the currency of this PlanData.
        Currency for plan.

        :return: The currency of this PlanData.
        :rtype: str
        """
        return self._currency

    @currency.setter
    def currency(self, currency):
        """
        Sets the currency of this PlanData.
        Currency for plan.

        :param currency: The currency of this PlanData.
        :type: str
        """

        self._currency = currency

    @property
    def interval(self):
        """
        Gets the interval of this PlanData.
        Plan interval.

        :return: The interval of this PlanData.
        :rtype: list[str]
        """
        return self._interval

    @interval.setter
    def interval(self, interval):
        """
        Sets the interval of this PlanData.
        Plan interval.

        :param interval: The interval of this PlanData.
        :type: list[str]
        """
        if interval is None:
            raise ValueError("Invalid value for `interval`, must not be `None`")
        allowed_values = ["day", "week", "month", "year"]
        if not set(interval).issubset(set(allowed_values)):
            raise ValueError(
                "Invalid values for `interval` [{0}], must be a subset of [{1}]"
                .format(", ".join(map(str, set(interval)-set(allowed_values))),
                        ", ".join(map(str, allowed_values)))
            )

        self._interval = interval

    @property
    def interval_count(self):
        """
        Gets the interval_count of this PlanData.
        Number of intervals.

        :return: The interval_count of this PlanData.
        :rtype: int
        """
        return self._interval_count

    @interval_count.setter
    def interval_count(self, interval_count):
        """
        Sets the interval_count of this PlanData.
        Number of intervals.

        :param interval_count: The interval_count of this PlanData.
        :type: int
        """
        if interval_count is None:
            raise ValueError("Invalid value for `interval_count`, must not be `None`")

        self._interval_count = interval_count

    @property
    def name(self):
        """
        Gets the name of this PlanData.
        Plan name.

        :return: The name of this PlanData.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """
        Sets the name of this PlanData.
        Plan name.

        :param name: The name of this PlanData.
        :type: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")

        self._name = name

    @property
    def statement_descriptor(self):
        """
        Gets the statement_descriptor of this PlanData.
        Plan description.

        :return: The statement_descriptor of this PlanData.
        :rtype: str
        """
        return self._statement_descriptor

    @statement_descriptor.setter
    def statement_descriptor(self, statement_descriptor):
        """
        Sets the statement_descriptor of this PlanData.
        Plan description.

        :param statement_descriptor: The statement_descriptor of this PlanData.
        :type: str
        """

        self._statement_descriptor = statement_descriptor

    @property
    def trial_period_days(self):
        """
        Gets the trial_period_days of this PlanData.
        Trial days for try and buy campaigns.

        :return: The trial_period_days of this PlanData.
        :rtype: int
        """
        return self._trial_period_days

    @trial_period_days.setter
    def trial_period_days(self, trial_period_days):
        """
        Sets the trial_period_days of this PlanData.
        Trial days for try and buy campaigns.

        :param trial_period_days: The trial_period_days of this PlanData.
        :type: int
        """

        self._trial_period_days = trial_period_days

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, PlanData):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
