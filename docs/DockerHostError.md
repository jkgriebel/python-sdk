# DockerHostError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**non_field_errors** | **list[str]** | Errors not connected to any field. | [optional] 
**id** | **list[str]** | Id field errors. | [optional] 
**name** | **list[str]** | Name field errors. | [optional] 
**ip** | **list[str]** | Ip field errors | [optional] 
**port** | **list[str]** | Port field errors. | [optional] 
**status** | **list[str]** | Status field errors. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


