# NotificationSettingsError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**non_field_errors** | **list[str]** | Errors not connected to any field | [optional] 
**enabled** | **bool** | enabled field errors | [optional] 
**emails_enabled** | **bool** | emails_enabled field errors | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


