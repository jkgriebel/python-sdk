# ServerRunStatistics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**server** | **str** | Server name. | [optional] 
**id** | **str** | Server run statistics object unique identifier in UUID format. | [optional] 
**start** | **str** | Date and time that represents when server started. | [optional] 
**stop** | **str** | Date and time that represents when server stopped. | [optional] 
**exit_code** | **int** | Exit code, such as Exit 0 or Exit 1. | [optional] 
**size** | **int** | Size of server. | [optional] 
**stacktrace** | **str** | Stacktrace, if there was one. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


