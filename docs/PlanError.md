# PlanError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**non_field_errors** | **list[str]** | Errors not connected to any field | [optional] 
**id** | **list[str]** | id field errors | [optional] 
**stripe_id** | **list[str]** | stripe_id field errors | [optional] 
**created** | **list[str]** | created field errors | [optional] 
**metadata** | **list[str]** | metadata field errors | [optional] 
**livemode** | **list[str]** | livemode field errors | [optional] 
**amount** | **list[str]** | amount field errors | [optional] 
**currency** | **list[str]** | currency field errors | [optional] 
**interval** | **list[str]** | interval field errors | [optional] 
**interval_count** | **list[str]** | interval_count field errors | [optional] 
**name** | **list[str]** | name field errors | [optional] 
**statement_descriptor** | **list[str]** | statement_descriptor field errors | [optional] 
**trial_period_days** | **list[str]** | trial period days field errors | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


