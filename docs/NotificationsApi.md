# swagger_client.NotificationsApi

All URIs are relative to *https://api.3blades.ai*

Method | HTTP request | Description
------------- | ------------- | -------------
[**notification_read**](NotificationsApi.md#notification_read) | **GET** /v1/{namespace}/notifications/{notification_id} | Retrieve a specific notification.
[**notification_settings_create**](NotificationsApi.md#notification_settings_create) | **POST** /v1/{namespace}/notifications/settings/ | Create global notification settings
[**notification_settings_entity_create**](NotificationsApi.md#notification_settings_entity_create) | **POST** /v1/{namespace}/notifications/settings/entity/{entity} | Create global notification settings
[**notification_settings_entity_read**](NotificationsApi.md#notification_settings_entity_read) | **GET** /v1/{namespace}/notifications/settings/entity/{entity} | Retrieve global notification settings for the authenticated user
[**notification_settings_entity_update**](NotificationsApi.md#notification_settings_entity_update) | **PATCH** /v1/{namespace}/notifications/settings/entity/{entity} | Modify global notification settings.
[**notification_settings_read**](NotificationsApi.md#notification_settings_read) | **GET** /v1/{namespace}/notifications/settings/ | Retrieve global notification settings for the authenticated user
[**notification_settings_update**](NotificationsApi.md#notification_settings_update) | **PATCH** /v1/{namespace}/notifications/settings/ | Modify global notification settings.
[**notification_update**](NotificationsApi.md#notification_update) | **PATCH** /v1/{namespace}/notifications/{notification_id} | Mark a specific notification as either read or unread.
[**notifications_list**](NotificationsApi.md#notifications_list) | **GET** /v1/{namespace}/notifications/ | Get notifications of all types and entities for the authenticated user.
[**notifications_list_entity**](NotificationsApi.md#notifications_list_entity) | **GET** /v1/{namespace}/notifications/entity/{entity} | Get notifications of all types and entities for the authenticated user.
[**notifications_update_entity_list**](NotificationsApi.md#notifications_update_entity_list) | **PATCH** /v1/{namespace}/notifications/entity/{entity} | Mark a list of notifications as either read or unread.
[**notifications_update_list**](NotificationsApi.md#notifications_update_list) | **PATCH** /v1/{namespace}/notifications/ | Mark a list of notifications as either read or unread.


# **notification_read**
> Notification notification_read(namespace, notification_id)

Retrieve a specific notification.

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.NotificationsApi()
namespace = 'namespace_example' # str | User or team data.
notification_id = 'notification_id_example' # str | Notification UUID.

try: 
    # Retrieve a specific notification.
    api_response = api_instance.notification_read(namespace, notification_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling NotificationsApi->notification_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team data. | 
 **notification_id** | **str**| Notification UUID. | 

### Return type

[**Notification**](Notification.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **notification_settings_create**
> NotificationSettings notification_settings_create(namespace, notification_settings_data)

Create global notification settings

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.NotificationsApi()
namespace = 'namespace_example' # str | User or team name.
notification_settings_data = swagger_client.NotificationSettingsData() # NotificationSettingsData | 

try: 
    # Create global notification settings
    api_response = api_instance.notification_settings_create(namespace, notification_settings_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling NotificationsApi->notification_settings_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **notification_settings_data** | [**NotificationSettingsData**](NotificationSettingsData.md)|  | 

### Return type

[**NotificationSettings**](NotificationSettings.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **notification_settings_entity_create**
> NotificationSettings notification_settings_entity_create(namespace, entity, notification_settings_data=notification_settings_data)

Create global notification settings

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.NotificationsApi()
namespace = 'namespace_example' # str | User or team name.
entity = 'entity_example' # str | Entity whose settings should be retrieved.
notification_settings_data = swagger_client.NotificationSettingsData() # NotificationSettingsData |  (optional)

try: 
    # Create global notification settings
    api_response = api_instance.notification_settings_entity_create(namespace, entity, notification_settings_data=notification_settings_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling NotificationsApi->notification_settings_entity_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **entity** | **str**| Entity whose settings should be retrieved. | 
 **notification_settings_data** | [**NotificationSettingsData**](NotificationSettingsData.md)|  | [optional] 

### Return type

[**NotificationSettings**](NotificationSettings.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **notification_settings_entity_read**
> list[NotificationSettings] notification_settings_entity_read(namespace, entity)

Retrieve global notification settings for the authenticated user

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.NotificationsApi()
namespace = 'namespace_example' # str | User or team data.
entity = 'entity_example' # str | Entity whose settings should be retrieved.

try: 
    # Retrieve global notification settings for the authenticated user
    api_response = api_instance.notification_settings_entity_read(namespace, entity)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling NotificationsApi->notification_settings_entity_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team data. | 
 **entity** | **str**| Entity whose settings should be retrieved. | 

### Return type

[**list[NotificationSettings]**](NotificationSettings.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **notification_settings_entity_update**
> NotificationSettings notification_settings_entity_update(namespace, entity, notification_settings_data=notification_settings_data)

Modify global notification settings.

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.NotificationsApi()
namespace = 'namespace_example' # str | User or team name.
entity = 'entity_example' # str | Entity whose settings should be retrieved.
notification_settings_data = swagger_client.NotificationSettingsData() # NotificationSettingsData |  (optional)

try: 
    # Modify global notification settings.
    api_response = api_instance.notification_settings_entity_update(namespace, entity, notification_settings_data=notification_settings_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling NotificationsApi->notification_settings_entity_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **entity** | **str**| Entity whose settings should be retrieved. | 
 **notification_settings_data** | [**NotificationSettingsData**](NotificationSettingsData.md)|  | [optional] 

### Return type

[**NotificationSettings**](NotificationSettings.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **notification_settings_read**
> list[NotificationSettings] notification_settings_read(namespace)

Retrieve global notification settings for the authenticated user

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.NotificationsApi()
namespace = 'namespace_example' # str | User or team data.

try: 
    # Retrieve global notification settings for the authenticated user
    api_response = api_instance.notification_settings_read(namespace)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling NotificationsApi->notification_settings_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team data. | 

### Return type

[**list[NotificationSettings]**](NotificationSettings.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **notification_settings_update**
> NotificationSettings notification_settings_update(namespace, notification_settings_data=notification_settings_data)

Modify global notification settings.

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.NotificationsApi()
namespace = 'namespace_example' # str | User or team name.
notification_settings_data = swagger_client.NotificationSettingsData() # NotificationSettingsData |  (optional)

try: 
    # Modify global notification settings.
    api_response = api_instance.notification_settings_update(namespace, notification_settings_data=notification_settings_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling NotificationsApi->notification_settings_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **notification_settings_data** | [**NotificationSettingsData**](NotificationSettingsData.md)|  | [optional] 

### Return type

[**NotificationSettings**](NotificationSettings.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **notification_update**
> Notification notification_update(namespace, notification_id, notification_data=notification_data)

Mark a specific notification as either read or unread.

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.NotificationsApi()
namespace = 'namespace_example' # str | User or team data.
notification_id = 'notification_id_example' # str | Notification UUID.
notification_data = swagger_client.NotificationUpdateData() # NotificationUpdateData |  (optional)

try: 
    # Mark a specific notification as either read or unread.
    api_response = api_instance.notification_update(namespace, notification_id, notification_data=notification_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling NotificationsApi->notification_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team data. | 
 **notification_id** | **str**| Notification UUID. | 
 **notification_data** | [**NotificationUpdateData**](NotificationUpdateData.md)|  | [optional] 

### Return type

[**Notification**](Notification.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **notifications_list**
> list[Notification] notifications_list(namespace, limit=limit, offset=offset, ordering=ordering, read=read)

Get notifications of all types and entities for the authenticated user.

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.NotificationsApi()
namespace = 'namespace_example' # str | User or team data.
limit = 'limit_example' # str | Limit when getting items. (optional)
offset = 'offset_example' # str | Offset when getting items. (optional)
ordering = 'ordering_example' # str | Ordering when getting items. (optional)
read = true # bool | When true, get only read notifications. When false, get only unread notifications. Default behavior is to return both read and unread. (optional)

try: 
    # Get notifications of all types and entities for the authenticated user.
    api_response = api_instance.notifications_list(namespace, limit=limit, offset=offset, ordering=ordering, read=read)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling NotificationsApi->notifications_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team data. | 
 **limit** | **str**| Limit when getting items. | [optional] 
 **offset** | **str**| Offset when getting items. | [optional] 
 **ordering** | **str**| Ordering when getting items. | [optional] 
 **read** | **bool**| When true, get only read notifications. When false, get only unread notifications. Default behavior is to return both read and unread. | [optional] 

### Return type

[**list[Notification]**](Notification.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **notifications_list_entity**
> list[Notification] notifications_list_entity(namespace, entity, limit=limit, offset=offset, ordering=ordering, read=read)

Get notifications of all types and entities for the authenticated user.

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.NotificationsApi()
namespace = 'namespace_example' # str | User or team data.
entity = 'entity_example' # str | Entity to filter notifications by.
limit = 'limit_example' # str | Limit when getting items. (optional)
offset = 'offset_example' # str | Offset when getting items. (optional)
ordering = 'ordering_example' # str | Ordering when getting items. (optional)
read = true # bool | When true, get only read notifications. When false, get only unread notifications. Default behavior is to return both read and unread. (optional)

try: 
    # Get notifications of all types and entities for the authenticated user.
    api_response = api_instance.notifications_list_entity(namespace, entity, limit=limit, offset=offset, ordering=ordering, read=read)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling NotificationsApi->notifications_list_entity: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team data. | 
 **entity** | **str**| Entity to filter notifications by. | 
 **limit** | **str**| Limit when getting items. | [optional] 
 **offset** | **str**| Offset when getting items. | [optional] 
 **ordering** | **str**| Ordering when getting items. | [optional] 
 **read** | **bool**| When true, get only read notifications. When false, get only unread notifications. Default behavior is to return both read and unread. | [optional] 

### Return type

[**list[Notification]**](Notification.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **notifications_update_entity_list**
> Notification notifications_update_entity_list(namespace, entity, notification_data)

Mark a list of notifications as either read or unread.

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.NotificationsApi()
namespace = 'namespace_example' # str | User or team name.
entity = 'entity_example' # str | Entity to filter notifications by.
notification_data = swagger_client.NotificationListUpdateData() # NotificationListUpdateData | 

try: 
    # Mark a list of notifications as either read or unread.
    api_response = api_instance.notifications_update_entity_list(namespace, entity, notification_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling NotificationsApi->notifications_update_entity_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **entity** | **str**| Entity to filter notifications by. | 
 **notification_data** | [**NotificationListUpdateData**](NotificationListUpdateData.md)|  | 

### Return type

[**Notification**](Notification.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **notifications_update_list**
> Notification notifications_update_list(namespace, notification_data)

Mark a list of notifications as either read or unread.

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.NotificationsApi()
namespace = 'namespace_example' # str | User or team name.
notification_data = swagger_client.NotificationListUpdateData() # NotificationListUpdateData | 

try: 
    # Mark a list of notifications as either read or unread.
    api_response = api_instance.notifications_update_list(namespace, notification_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling NotificationsApi->notifications_update_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **notification_data** | [**NotificationListUpdateData**](NotificationListUpdateData.md)|  | 

### Return type

[**Notification**](Notification.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

