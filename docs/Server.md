# Server

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Server unique identifier in UUID format. | [optional] 
**name** | **str** | Server name. | 
**project** | **str** | Project name. | [optional] 
**created_at** | **str** | Date and time when server was created. | [optional] 
**created_by** | **str** | User that created server. | [optional] 
**image_name** | **str** | Server image source, such as 3blades/tensorflow-notebook.  | [optional] 
**server_size** | **str** | Server size unique identifier. | [optional] 
**startup_script** | **str** | Optional startup script to use when launching server. | [optional] 
**config** | **object** | Server configuration option. Values are jupyter, restful and cron. | [optional] 
**status** | **str** | Server status, such as Running or Error. | [optional] 
**connected** | **list[str]** | Array that represents what other servers the server is connected to. | [optional] 
**host** | **str** | Value that represents user defined host, otherwise known as BYON (Bring Your Own Node).  | [optional] 
**endpoint** | **str** | Server endpoint path. | [optional] 
**logs_url** | **str** | A WebSocket URL for streaming stdout and stderr logs from the server.  | [optional] 
**status_url** | **str** | A WebSocket URL for listening to server status changes.  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


