# UserData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **str** | Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only. | 
**email** | **str** | User email. | [optional] 
**first_name** | **str** | User first name. | [optional] 
**last_name** | **str** | User last name. | [optional] 
**password** | **str** | User password. | 
**profile** | [**UserProfile**](UserProfile.md) | User profile information is required, although attribute values may be empty. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


