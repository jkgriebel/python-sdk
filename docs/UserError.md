# UserError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**non_field_errors** | **list[str]** | Errors not connected to any field. | [optional] 
**id** | **list[str]** | id field errors. | [optional] 
**username** | **list[str]** | Username field errors. | [optional] 
**email** | **list[str]** | Email field errors. | [optional] 
**first_name** | **list[str]** | First name field errors. | [optional] 
**last_name** | **list[str]** | Last name field errors. | [optional] 
**password** | **list[str]** | Password field errors. | [optional] 
**profile** | [**UserProfileError**](UserProfileError.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


