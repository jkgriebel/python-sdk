# Email

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Email unique identifier expressed as UUID. | [optional] 
**user** | **str** | User name for email. | [optional] 
**address** | **str** | Email address. | 
**public** | **bool** | Boolean to determine if email is public or private. | [optional] 
**unsubscribed** | **bool** | Boolean to determine whether user is suscribed or unsubscribed to out going email campaigns. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


