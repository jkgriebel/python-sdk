# swagger_client.UsersApi

All URIs are relative to *https://api.3blades.ai*

Method | HTTP request | Description
------------- | ------------- | -------------
[**me**](UsersApi.md#me) | **GET** /v1/me | A convenience endpoint that is equivalent to GET /v1/users/profiles/&lt;my user id&gt;/
[**user_avatar_delete**](UsersApi.md#user_avatar_delete) | **DELETE** /v1/users/{user}/avatar/ | Delete avatar
[**user_avatar_get**](UsersApi.md#user_avatar_get) | **GET** /v1/users/{user}/avatar/ | Retrieve user&#39;s avatar
[**user_avatar_set**](UsersApi.md#user_avatar_set) | **POST** /v1/users/{user}/avatar/ | Add user avatar
[**user_avatar_update**](UsersApi.md#user_avatar_update) | **PATCH** /v1/users/{user}/avatar/ | Update a project file
[**users_api_key_list**](UsersApi.md#users_api_key_list) | **GET** /v1/users/{user}/api-key/ | Retrieve account&#39;s API key
[**users_api_key_reset**](UsersApi.md#users_api_key_reset) | **POST** /v1/users/{user}/api-key/reset/ | Reset a user&#39;s API key
[**users_create**](UsersApi.md#users_create) | **POST** /v1/users/profiles/ | Create new user
[**users_delete**](UsersApi.md#users_delete) | **DELETE** /v1/users/profiles/{user}/ | Delete a user
[**users_emails_create**](UsersApi.md#users_emails_create) | **POST** /v1/users/{user}/emails/ | Create an email address
[**users_emails_delete**](UsersApi.md#users_emails_delete) | **DELETE** /v1/users/{user}/emails/{email_id}/ | Delete an email address
[**users_emails_list**](UsersApi.md#users_emails_list) | **GET** /v1/users/{user}/emails/ | Retrieve account email addresses
[**users_emails_read**](UsersApi.md#users_emails_read) | **GET** /v1/users/{user}/emails/{email_id}/ | Retrieve a user&#39;s email addresses
[**users_emails_replace**](UsersApi.md#users_emails_replace) | **PUT** /v1/users/{user}/emails/{email_id}/ | Replace an email address
[**users_emails_update**](UsersApi.md#users_emails_update) | **PATCH** /v1/users/{user}/emails/{email_id}/ | Update an email address
[**users_list**](UsersApi.md#users_list) | **GET** /v1/users/profiles/ | Get user list
[**users_read**](UsersApi.md#users_read) | **GET** /v1/users/profiles/{user}/ | Retrieve a user
[**users_ssh_key_list**](UsersApi.md#users_ssh_key_list) | **GET** /v1/users/{user}/ssh-key/ | Retrieve an SSH key
[**users_ssh_key_reset**](UsersApi.md#users_ssh_key_reset) | **POST** /v1/users/{user}/ssh-key/reset/ | Recreate an SSH key
[**users_update**](UsersApi.md#users_update) | **PATCH** /v1/users/profiles/{user}/ | Update a user


# **me**
> User me()

A convenience endpoint that is equivalent to GET /v1/users/profiles/<my user id>/

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.UsersApi()

try: 
    # A convenience endpoint that is equivalent to GET /v1/users/profiles/<my user id>/
    api_response = api_instance.me()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UsersApi->me: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**User**](User.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **user_avatar_delete**
> user_avatar_delete(user)

Delete avatar

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.UsersApi()
user = 'user_example' # str | User unique identifier expressed as UUID or username.

try: 
    # Delete avatar
    api_instance.user_avatar_delete(user)
except ApiException as e:
    print("Exception when calling UsersApi->user_avatar_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **str**| User unique identifier expressed as UUID or username. | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **user_avatar_get**
> user_avatar_get(user)

Retrieve user's avatar

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.UsersApi()
user = 'user_example' # str | User unique identifier expressed as UUIDor username.

try: 
    # Retrieve user's avatar
    api_instance.user_avatar_get(user)
except ApiException as e:
    print("Exception when calling UsersApi->user_avatar_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **str**| User unique identifier expressed as UUIDor username. | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **user_avatar_set**
> User user_avatar_set(user)

Add user avatar

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.UsersApi()
user = 'user_example' # str | User unique identifier expressed as UUID or username.

try: 
    # Add user avatar
    api_response = api_instance.user_avatar_set(user)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UsersApi->user_avatar_set: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **str**| User unique identifier expressed as UUID or username. | 

### Return type

[**User**](User.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **user_avatar_update**
> User user_avatar_update(user)

Update a project file

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.UsersApi()
user = 'user_example' # str | User unique identifier expressed as UUID or username.

try: 
    # Update a project file
    api_response = api_instance.user_avatar_update(user)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UsersApi->user_avatar_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **str**| User unique identifier expressed as UUID or username. | 

### Return type

[**User**](User.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_api_key_list**
> users_api_key_list(user)

Retrieve account's API key

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.UsersApi()
user = 'user_example' # str | User unique identifier expressed as UUID or username.

try: 
    # Retrieve account's API key
    api_instance.users_api_key_list(user)
except ApiException as e:
    print("Exception when calling UsersApi->users_api_key_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **str**| User unique identifier expressed as UUID or username. | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_api_key_reset**
> users_api_key_reset(user)

Reset a user's API key

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.UsersApi()
user = 'user_example' # str | User unique identifier expressed as UUID or username.

try: 
    # Reset a user's API key
    api_instance.users_api_key_reset(user)
except ApiException as e:
    print("Exception when calling UsersApi->users_api_key_reset: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **str**| User unique identifier expressed as UUID or username. | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_create**
> User users_create(user_data=user_data)

Create new user

Only admin users can create new users. New users have active status by default.

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.UsersApi()
user_data = swagger_client.UserData() # UserData |  (optional)

try: 
    # Create new user
    api_response = api_instance.users_create(user_data=user_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UsersApi->users_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_data** | [**UserData**](UserData.md)|  | [optional] 

### Return type

[**User**](User.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_delete**
> users_delete(user)

Delete a user

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.UsersApi()
user = 'user_example' # str | User identifier expressed as UUID or username.

try: 
    # Delete a user
    api_instance.users_delete(user)
except ApiException as e:
    print("Exception when calling UsersApi->users_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **str**| User identifier expressed as UUID or username. | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_emails_create**
> Email users_emails_create(user, email_data=email_data)

Create an email address

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.UsersApi()
user = 'user_example' # str | User unique identifier expressed as UUID or username.
email_data = swagger_client.EmailData() # EmailData |  (optional)

try: 
    # Create an email address
    api_response = api_instance.users_emails_create(user, email_data=email_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UsersApi->users_emails_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **str**| User unique identifier expressed as UUID or username. | 
 **email_data** | [**EmailData**](EmailData.md)|  | [optional] 

### Return type

[**Email**](Email.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_emails_delete**
> users_emails_delete(email_id, user)

Delete an email address

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.UsersApi()
email_id = 'email_id_example' # str | Email unique identifier expressed as UUID.
user = 'user_example' # str | User unique identifier expressed as UUID or username.

try: 
    # Delete an email address
    api_instance.users_emails_delete(email_id, user)
except ApiException as e:
    print("Exception when calling UsersApi->users_emails_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email_id** | **str**| Email unique identifier expressed as UUID. | 
 **user** | **str**| User unique identifier expressed as UUID or username. | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_emails_list**
> list[Email] users_emails_list(user, limit=limit, offset=offset, ordering=ordering)

Retrieve account email addresses

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.UsersApi()
user = 'user_example' # str | User unique identifier as expressed as UUID or username.
limit = 'limit_example' # str | Limite when getting email list. (optional)
offset = 'offset_example' # str | Offset when getting email list. (optional)
ordering = 'ordering_example' # str | Ordering when getting email list. (optional)

try: 
    # Retrieve account email addresses
    api_response = api_instance.users_emails_list(user, limit=limit, offset=offset, ordering=ordering)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UsersApi->users_emails_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **str**| User unique identifier as expressed as UUID or username. | 
 **limit** | **str**| Limite when getting email list. | [optional] 
 **offset** | **str**| Offset when getting email list. | [optional] 
 **ordering** | **str**| Ordering when getting email list. | [optional] 

### Return type

[**list[Email]**](Email.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_emails_read**
> Email users_emails_read(email_id, user)

Retrieve a user's email addresses

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.UsersApi()
email_id = 'email_id_example' # str | Email unique identifier expressed as UUID.
user = 'user_example' # str | User unique identifier expressed as UUID or username.

try: 
    # Retrieve a user's email addresses
    api_response = api_instance.users_emails_read(email_id, user)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UsersApi->users_emails_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email_id** | **str**| Email unique identifier expressed as UUID. | 
 **user** | **str**| User unique identifier expressed as UUID or username. | 

### Return type

[**Email**](Email.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_emails_replace**
> Email users_emails_replace(email_id, user, email_data=email_data)

Replace an email address

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.UsersApi()
email_id = 'email_id_example' # str | Email unique identifier expressed as UUID.
user = 'user_example' # str | User unique identifier expressed as UUID or username.
email_data = swagger_client.EmailData() # EmailData |  (optional)

try: 
    # Replace an email address
    api_response = api_instance.users_emails_replace(email_id, user, email_data=email_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UsersApi->users_emails_replace: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email_id** | **str**| Email unique identifier expressed as UUID. | 
 **user** | **str**| User unique identifier expressed as UUID or username. | 
 **email_data** | [**EmailData**](EmailData.md)|  | [optional] 

### Return type

[**Email**](Email.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_emails_update**
> Email users_emails_update(email_id, user, email_data=email_data)

Update an email address

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.UsersApi()
email_id = 'email_id_example' # str | Email unique identifier expressed as UUID.
user = 'user_example' # str | User unique identifier expressed as UUID or username.
email_data = swagger_client.EmailData() # EmailData |  (optional)

try: 
    # Update an email address
    api_response = api_instance.users_emails_update(email_id, user, email_data=email_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UsersApi->users_emails_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email_id** | **str**| Email unique identifier expressed as UUID. | 
 **user** | **str**| User unique identifier expressed as UUID or username. | 
 **email_data** | [**EmailData**](EmailData.md)|  | [optional] 

### Return type

[**Email**](Email.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_list**
> list[User] users_list(limit=limit, offset=offset, username=username, email=email, ordering=ordering)

Get user list

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.UsersApi()
limit = 'limit_example' # str | Limit user list. (optional)
offset = 'offset_example' # str | Offset when getting users. (optional)
username = 'username_example' # str | User username. (optional)
email = 'email_example' # str | User email. (optional)
ordering = 'ordering_example' # str | Ordering when getting users. (optional)

try: 
    # Get user list
    api_response = api_instance.users_list(limit=limit, offset=offset, username=username, email=email, ordering=ordering)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UsersApi->users_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **str**| Limit user list. | [optional] 
 **offset** | **str**| Offset when getting users. | [optional] 
 **username** | **str**| User username. | [optional] 
 **email** | **str**| User email. | [optional] 
 **ordering** | **str**| Ordering when getting users. | [optional] 

### Return type

[**list[User]**](User.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_read**
> User users_read(user)

Retrieve a user

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.UsersApi()
user = 'user_example' # str | Unique identifier expressed as UUID or username.

try: 
    # Retrieve a user
    api_response = api_instance.users_read(user)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UsersApi->users_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **str**| Unique identifier expressed as UUID or username. | 

### Return type

[**User**](User.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_ssh_key_list**
> users_ssh_key_list(user)

Retrieve an SSH key

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.UsersApi()
user = 'user_example' # str | User unique identifier expressed as UUID or username.

try: 
    # Retrieve an SSH key
    api_instance.users_ssh_key_list(user)
except ApiException as e:
    print("Exception when calling UsersApi->users_ssh_key_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **str**| User unique identifier expressed as UUID or username. | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_ssh_key_reset**
> users_ssh_key_reset(user)

Recreate an SSH key

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.UsersApi()
user = 'user_example' # str | User unique identifier expressed as UUID or username.

try: 
    # Recreate an SSH key
    api_instance.users_ssh_key_reset(user)
except ApiException as e:
    print("Exception when calling UsersApi->users_ssh_key_reset: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **str**| User unique identifier expressed as UUID or username. | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_update**
> User users_update(user, user_data=user_data)

Update a user

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.UsersApi()
user = 'user_example' # str | User unique identifier expressed as UUID or username.
user_data = swagger_client.UserData() # UserData |  (optional)

try: 
    # Update a user
    api_response = api_instance.users_update(user, user_data=user_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UsersApi->users_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **str**| User unique identifier expressed as UUID or username. | 
 **user_data** | [**UserData**](UserData.md)|  | [optional] 

### Return type

[**User**](User.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

