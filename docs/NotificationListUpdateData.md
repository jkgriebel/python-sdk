# NotificationListUpdateData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**notifications** | **list[str]** | An array of notification IDs to update. | 
**read** | **bool** | Mark the notification as either read or unread | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


