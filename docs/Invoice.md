# Invoice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Invoice unique identifier expressed as UUID. | [optional] 
**stripe_id** | **str** | Stripe account identifier. | 
**created** | **str** | Date and time when invoice was created. | 
**metadata** | **object** | Optional metadata object of invoice. | [optional] 
**livemode** | **bool** | Boolean that determines whether invoice is live, or not. | [optional] 
**amount_due** | **int** | Amount due set in invoice. | 
**application_fee** | **int** | Application fee set in invoice. | [optional] 
**attempt_count** | **int** | Number of attempts to deliver invoice. | [optional] 
**attempted** | **bool** | Boolean to determine whether delivery attempt executed, or not. | [optional] 
**closed** | **bool** | Invoice closed, or pending. | [optional] 
**currency** | **str** | Currency used in invoice. | 
**invoice_date** | **str** | Invoice issue date. | 
**description** | **str** | Invoice description. | [optional] 
**next_payment_attempt** | **str** | Next payment attempt. | [optional] 
**paid** | **bool** | Determines whether invoice has been paid, or not. | [optional] 
**period_start** | **str** | Invoice start period. | 
**period_end** | **str** | Invoice end period. | 
**reciept_number** | **str** | Invoice receipt number. | 
**starting_balance** | **int** | Invoice starting balance. | 
**statement_descriptor** | **str** | Invoice statement descriptor. | [optional] 
**subtotal** | **int** | Invoice sub total. | 
**tax** | **int** | Tax, if applicable. | [optional] 
**total** | **int** | Invoice total. | 
**customer** | **str** | Customer name. | 
**subscription** | **str** | Suscription name. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


