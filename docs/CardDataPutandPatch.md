# CardDataPutandPatch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Card name. | [optional] 
**address_line1** | **str** | Address line 1. | [optional] 
**address_line2** | **str** | Address line 2. | [optional] 
**address_city** | **str** | Address city. | [optional] 
**address_state** | **str** | Address state. | [optional] 
**address_zip** | **str** | Address zip code. | [optional] 
**address_country** | **str** | Address country. | [optional] 
**exp_month** | **int** | Card expiration month. | [optional] 
**exp_year** | **int** | Card expiration year. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


