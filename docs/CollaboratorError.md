# CollaboratorError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**non_field_errors** | **list[str]** | Errors not connected to any field. | [optional] 
**id** | **list[str]** | Id field errors this | [optional] 
**owner** | **list[str]** | Owner field errors. | [optional] 
**joined** | **list[str]** | Joined field errors. | [optional] 
**username** | **list[str]** | Username field errors. | [optional] 
**email** | **list[str]** | Email field errors. | [optional] 
**first_name** | **list[str]** | First name field errors. | [optional] 
**last_name** | **list[str]** | Last name field errors. | [optional] 
**member** | **list[str]** | Member field errors. | 
**permissions** | **list[str]** | Permissions field errors. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


