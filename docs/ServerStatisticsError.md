# ServerStatisticsError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**non_field_errors** | **list[str]** | Errors not connected to any field. | [optional] 
**id** | **list[str]** | Id field errors. | [optional] 
**start** | **list[str]** | Start field errors. | [optional] 
**stop** | **list[str]** | Stop field errors. | [optional] 
**size** | **list[str]** | Size field errors. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


