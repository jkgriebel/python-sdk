# ServerData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Server name. | 
**image_name** | **str** | Image name. | [optional] 
**server_size** | **str** | Server size unique identifier. | [optional] 
**startup_script** | **str** | Startup script to run when launching server. | [optional] 
**config** | [**ServerConfig**](ServerConfig.md) | Server configuration option. Values are jupyter, restful and cron. | [optional] 
**connected** | **list[str]** | Array of other servers the server is connected to. | [optional] 
**host** | **str** | External host IPv4 address or hostname. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


