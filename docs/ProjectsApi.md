# swagger_client.ProjectsApi

All URIs are relative to *https://api.3blades.ai*

Method | HTTP request | Description
------------- | ------------- | -------------
[**project_copy**](ProjectsApi.md#project_copy) | **POST** /v1/{namespace}/projects/project-copy/ | Copy a project to your own account.
[**project_copy_check**](ProjectsApi.md#project_copy_check) | **HEAD** /v1/{namespace}/projects/project-copy-check/ | Check if you are able to copy a project to your account.
[**projects_collaborators_create**](ProjectsApi.md#projects_collaborators_create) | **POST** /v1/{namespace}/projects/{project}/collaborators/ | Create project collaborators
[**projects_collaborators_delete**](ProjectsApi.md#projects_collaborators_delete) | **DELETE** /v1/{namespace}/projects/{project}/collaborators/{collaborator}/ | Delete a project collaborator
[**projects_collaborators_list**](ProjectsApi.md#projects_collaborators_list) | **GET** /v1/{namespace}/projects/{project}/collaborators/ | Get project collaborators
[**projects_collaborators_read**](ProjectsApi.md#projects_collaborators_read) | **GET** /v1/{namespace}/projects/{project}/collaborators/{collaborator}/ | Get a project collaborator
[**projects_collaborators_update**](ProjectsApi.md#projects_collaborators_update) | **PATCH** /v1/{namespace}/projects/{project}/collaborators/{collaborator}/ | Update project collaborator
[**projects_create**](ProjectsApi.md#projects_create) | **POST** /v1/{namespace}/projects/ | Create a new project
[**projects_delete**](ProjectsApi.md#projects_delete) | **DELETE** /v1/{namespace}/projects/{project}/ | Delete a project
[**projects_list**](ProjectsApi.md#projects_list) | **GET** /v1/{namespace}/projects/ | Get available projects
[**projects_project_files_create**](ProjectsApi.md#projects_project_files_create) | **POST** /v1/{namespace}/projects/{project}/project_files/ | Create project files
[**projects_project_files_delete**](ProjectsApi.md#projects_project_files_delete) | **DELETE** /v1/{namespace}/projects/{project}/project_files/{id}/ | Delete a project file
[**projects_project_files_list**](ProjectsApi.md#projects_project_files_list) | **GET** /v1/{namespace}/projects/{project}/project_files/ | Get project files
[**projects_project_files_read**](ProjectsApi.md#projects_project_files_read) | **GET** /v1/{namespace}/projects/{project}/project_files/{id}/ | Get a project file
[**projects_project_files_replace**](ProjectsApi.md#projects_project_files_replace) | **PUT** /v1/{namespace}/projects/{project}/project_files/{id}/ | Replace a project file
[**projects_project_files_update**](ProjectsApi.md#projects_project_files_update) | **PATCH** /v1/{namespace}/projects/{project}/project_files/{id}/ | Update a project file
[**projects_read**](ProjectsApi.md#projects_read) | **GET** /v1/{namespace}/projects/{project}/ | Get a project
[**projects_replace**](ProjectsApi.md#projects_replace) | **PUT** /v1/{namespace}/projects/{project}/ | Replace a project
[**projects_servers_api_key**](ProjectsApi.md#projects_servers_api_key) | **GET** /v1/{namespace}/projects/{project}/servers/{server}/api-key/ | Get server API key
[**projects_servers_auth**](ProjectsApi.md#projects_servers_auth) | **POST** /v1/{namespace}/projects/{project}/servers/{server}/auth/ | Server api key validation
[**projects_servers_create**](ProjectsApi.md#projects_servers_create) | **POST** /v1/{namespace}/projects/{project}/servers/ | Create a new server
[**projects_servers_delete**](ProjectsApi.md#projects_servers_delete) | **DELETE** /v1/{namespace}/projects/{project}/servers/{server}/ | Delete a server
[**projects_servers_list**](ProjectsApi.md#projects_servers_list) | **GET** /v1/{namespace}/projects/{project}/servers/ | Retrieve servers
[**projects_servers_read**](ProjectsApi.md#projects_servers_read) | **GET** /v1/{namespace}/projects/{project}/servers/{server}/ | Retrieve a server
[**projects_servers_replace**](ProjectsApi.md#projects_servers_replace) | **PUT** /v1/{namespace}/projects/{project}/servers/{server}/ | Replace a server
[**projects_servers_run_stats_create**](ProjectsApi.md#projects_servers_run_stats_create) | **POST** /v1/{namespace}/projects/{project}/servers/{server}/run-stats/ | Create a new server&#39;s run statistics
[**projects_servers_run_stats_delete**](ProjectsApi.md#projects_servers_run_stats_delete) | **DELETE** /v1/{namespace}/projects/{project}/servers/{server}/run-stats/{id}/ | Delete a server&#39;s statistics
[**projects_servers_run_stats_read**](ProjectsApi.md#projects_servers_run_stats_read) | **GET** /v1/{namespace}/projects/{project}/servers/{server}/run-stats/{id}/ | Retrieve statistics for a server
[**projects_servers_run_stats_replace**](ProjectsApi.md#projects_servers_run_stats_replace) | **PUT** /v1/{namespace}/projects/{project}/servers/{server}/run-stats/{id}/ | Replace a server&#39;s statistics
[**projects_servers_run_stats_update**](ProjectsApi.md#projects_servers_run_stats_update) | **PATCH** /v1/{namespace}/projects/{project}/servers/{server}/run-stats/{id}/ | Update a server&#39;s statistics
[**projects_servers_ssh_tunnels_create**](ProjectsApi.md#projects_servers_ssh_tunnels_create) | **POST** /v1/{namespace}/projects/{project}/servers/{server}/ssh-tunnels/ | Create SSH Tunnel associated to a server
[**projects_servers_ssh_tunnels_delete**](ProjectsApi.md#projects_servers_ssh_tunnels_delete) | **DELETE** /v1/{namespace}/projects/{project}/servers/{server}/ssh-tunnels/{tunnel}/ | Delete an SSH Tunnel associated to a server
[**projects_servers_ssh_tunnels_list**](ProjectsApi.md#projects_servers_ssh_tunnels_list) | **GET** /v1/{namespace}/projects/{project}/servers/{server}/ssh-tunnels/ | Get SSH Tunnels associated to a server
[**projects_servers_ssh_tunnels_read**](ProjectsApi.md#projects_servers_ssh_tunnels_read) | **GET** /v1/{namespace}/projects/{project}/servers/{server}/ssh-tunnels/{tunnel}/ | Get an SSH Tunnel associated to a server
[**projects_servers_ssh_tunnels_replace**](ProjectsApi.md#projects_servers_ssh_tunnels_replace) | **PUT** /v1/{namespace}/projects/{project}/servers/{server}/ssh-tunnels/{tunnel}/ | Replace SSH Tunnel associated to a server
[**projects_servers_ssh_tunnels_update**](ProjectsApi.md#projects_servers_ssh_tunnels_update) | **PATCH** /v1/{namespace}/projects/{project}/servers/{server}/ssh-tunnels/{tunnel}/ | Update an SSH Tunnel associated to a server
[**projects_servers_start**](ProjectsApi.md#projects_servers_start) | **POST** /v1/{namespace}/projects/{project}/servers/{server}/start/ | Start a server
[**projects_servers_stats_delete**](ProjectsApi.md#projects_servers_stats_delete) | **DELETE** /v1/{namespace}/projects/{project}/servers/{server}/stats/{id}/ | Delete a server&#39;s statistics
[**projects_servers_stats_read**](ProjectsApi.md#projects_servers_stats_read) | **GET** /v1/{namespace}/projects/{project}/servers/{server}/stats/{id}/ | Retrieve a server&#39;s statistics
[**projects_servers_stats_replace**](ProjectsApi.md#projects_servers_stats_replace) | **PUT** /v1/{namespace}/projects/{project}/servers/{server}/stats/{id}/ | Replace a server&#39;s statistics
[**projects_servers_stats_update**](ProjectsApi.md#projects_servers_stats_update) | **PATCH** /v1/{namespace}/projects/{project}/servers/{server}/stats/{id}/ | Update a server&#39;s statistics
[**projects_servers_stop**](ProjectsApi.md#projects_servers_stop) | **POST** /v1/{namespace}/projects/{project}/servers/{server}/stop/ | Stop a server
[**projects_servers_update**](ProjectsApi.md#projects_servers_update) | **PATCH** /v1/{namespace}/projects/{project}/servers/{server}/ | Update a server
[**projects_update**](ProjectsApi.md#projects_update) | **PATCH** /v1/{namespace}/projects/{project}/ | Update a project
[**service_trigger_create**](ProjectsApi.md#service_trigger_create) | **POST** /v1/{namespace}/projects/{project}/servers/{server}/triggers/ | Create a new server trigger
[**service_trigger_delete**](ProjectsApi.md#service_trigger_delete) | **DELETE** /v1/{namespace}/projects/{project}/servers/{server}/triggers/{trigger}/ | Delete a server trigger
[**service_trigger_list**](ProjectsApi.md#service_trigger_list) | **GET** /v1/{namespace}/projects/{project}/servers/{server}/triggers/ | Retrieve server triggers
[**service_trigger_read**](ProjectsApi.md#service_trigger_read) | **GET** /v1/{namespace}/projects/{project}/servers/{server}/triggers/{trigger}/ | Get a server trigger
[**service_trigger_replace**](ProjectsApi.md#service_trigger_replace) | **PUT** /v1/{namespace}/projects/{project}/servers/{server}/triggers/{trigger}/ | Replace a server trigger
[**service_trigger_update**](ProjectsApi.md#service_trigger_update) | **PATCH** /v1/{namespace}/projects/{project}/servers/{server}/triggers/{trigger}/ | Update a server trigger


# **project_copy**
> Project project_copy(namespace, project_copy_data)

Copy a project to your own account.

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
namespace = 'namespace_example' # str | User or team name.
project_copy_data = swagger_client.ProjectCopyData() # ProjectCopyData | 

try: 
    # Copy a project to your own account.
    api_response = api_instance.project_copy(namespace, project_copy_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->project_copy: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **project_copy_data** | [**ProjectCopyData**](ProjectCopyData.md)|  | 

### Return type

[**Project**](Project.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **project_copy_check**
> project_copy_check(namespace, project_copy_data)

Check if you are able to copy a project to your account.

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
namespace = 'namespace_example' # str | User or team name.
project_copy_data = swagger_client.ProjectCopyData1() # ProjectCopyData1 | 

try: 
    # Check if you are able to copy a project to your account.
    api_instance.project_copy_check(namespace, project_copy_data)
except ApiException as e:
    print("Exception when calling ProjectsApi->project_copy_check: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **project_copy_data** | [**ProjectCopyData1**](ProjectCopyData1.md)|  | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_collaborators_create**
> Collaborator projects_collaborators_create(project, namespace, collaborator_data=collaborator_data)

Create project collaborators

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
collaborator_data = swagger_client.CollaboratorData() # CollaboratorData |  (optional)

try: 
    # Create project collaborators
    api_response = api_instance.projects_collaborators_create(project, namespace, collaborator_data=collaborator_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_collaborators_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **collaborator_data** | [**CollaboratorData**](CollaboratorData.md)|  | [optional] 

### Return type

[**Collaborator**](Collaborator.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_collaborators_delete**
> projects_collaborators_delete(project, namespace, collaborator)

Delete a project collaborator

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
project = 'project_example' # str | Project unique identifier.
namespace = 'namespace_example' # str | User or team name.
collaborator = 'collaborator_example' # str | Collaborator unique identifier.

try: 
    # Delete a project collaborator
    api_instance.projects_collaborators_delete(project, namespace, collaborator)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_collaborators_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | **str**| Project unique identifier. | 
 **namespace** | **str**| User or team name. | 
 **collaborator** | **str**| Collaborator unique identifier. | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_collaborators_list**
> list[Collaborator] projects_collaborators_list(project, namespace, limit=limit, offset=offset, ordering=ordering)

Get project collaborators

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
limit = 'limit_example' # str | Limit when retrieving items. (optional)
offset = 'offset_example' # str | Offset when retrieving items. (optional)
ordering = 'ordering_example' # str | Ordering when retrieving items. (optional)

try: 
    # Get project collaborators
    api_response = api_instance.projects_collaborators_list(project, namespace, limit=limit, offset=offset, ordering=ordering)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_collaborators_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **limit** | **str**| Limit when retrieving items. | [optional] 
 **offset** | **str**| Offset when retrieving items. | [optional] 
 **ordering** | **str**| Ordering when retrieving items. | [optional] 

### Return type

[**list[Collaborator]**](Collaborator.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_collaborators_read**
> Collaborator projects_collaborators_read(project, namespace, collaborator)

Get a project collaborator

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
project = 'project_example' # str | Project unique identifier.
namespace = 'namespace_example' # str | User or team name.
collaborator = 'collaborator_example' # str | Collaborator unique identifier expressed as UUID or name.

try: 
    # Get a project collaborator
    api_response = api_instance.projects_collaborators_read(project, namespace, collaborator)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_collaborators_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | **str**| Project unique identifier. | 
 **namespace** | **str**| User or team name. | 
 **collaborator** | **str**| Collaborator unique identifier expressed as UUID or name. | 

### Return type

[**Collaborator**](Collaborator.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_collaborators_update**
> Collaborator projects_collaborators_update(project, namespace, collaborator, collaborator_data=collaborator_data)

Update project collaborator

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
project = 'project_example' # str | 
namespace = 'namespace_example' # str | User or team name.
collaborator = 'collaborator_example' # str | 
collaborator_data = swagger_client.CollaboratorData() # CollaboratorData |  (optional)

try: 
    # Update project collaborator
    api_response = api_instance.projects_collaborators_update(project, namespace, collaborator, collaborator_data=collaborator_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_collaborators_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | **str**|  | 
 **namespace** | **str**| User or team name. | 
 **collaborator** | **str**|  | 
 **collaborator_data** | [**CollaboratorData**](CollaboratorData.md)|  | [optional] 

### Return type

[**Collaborator**](Collaborator.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_create**
> Project projects_create(namespace, project_data=project_data)

Create a new project

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
namespace = 'namespace_example' # str | User or team name.
project_data = swagger_client.ProjectData() # ProjectData |  (optional)

try: 
    # Create a new project
    api_response = api_instance.projects_create(namespace, project_data=project_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **project_data** | [**ProjectData**](ProjectData.md)|  | [optional] 

### Return type

[**Project**](Project.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_delete**
> projects_delete(namespace, project)

Delete a project

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
namespace = 'namespace_example' # str | User or team name.
project = 'project_example' # str | Project unique identifier expressed as UUID or name.

try: 
    # Delete a project
    api_instance.projects_delete(namespace, project)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **project** | **str**| Project unique identifier expressed as UUID or name. | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_list**
> list[Project] projects_list(namespace, limit=limit, offset=offset, private=private, name=name, ordering=ordering)

Get available projects

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
namespace = 'namespace_example' # str | User or team name.
limit = 'limit_example' # str | Limit when getting data. (optional)
offset = 'offset_example' # str | Offset when getting data. (optional)
private = 'private_example' # str | Private project or public project. (optional)
name = 'name_example' # str | Project name. (optional)
ordering = 'ordering_example' # str | Ordering when getting projects. (optional)

try: 
    # Get available projects
    api_response = api_instance.projects_list(namespace, limit=limit, offset=offset, private=private, name=name, ordering=ordering)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **limit** | **str**| Limit when getting data. | [optional] 
 **offset** | **str**| Offset when getting data. | [optional] 
 **private** | **str**| Private project or public project. | [optional] 
 **name** | **str**| Project name. | [optional] 
 **ordering** | **str**| Ordering when getting projects. | [optional] 

### Return type

[**list[Project]**](Project.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_project_files_create**
> ProjectFile projects_project_files_create(project, namespace, file=file, base64_data=base64_data, name=name, path=path)

Create project files

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
project = 'project_example' # str | Project unique identifier.
namespace = 'namespace_example' # str | User or team name.
file = '/path/to/file.txt' # file | File to send, to create new file. This parameter is only used with form data and may include multiple files. (optional)
base64_data = 'base64_data_example' # str | Fila data, represented as base64. (optional)
name = 'name_example' # str | File name. May include path when creating file with base64 field. (optional)
path = 'path_example' # str | File path. Defaults to (/). (optional)

try: 
    # Create project files
    api_response = api_instance.projects_project_files_create(project, namespace, file=file, base64_data=base64_data, name=name, path=path)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_project_files_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | **str**| Project unique identifier. | 
 **namespace** | **str**| User or team name. | 
 **file** | **file**| File to send, to create new file. This parameter is only used with form data and may include multiple files. | [optional] 
 **base64_data** | **str**| Fila data, represented as base64. | [optional] 
 **name** | **str**| File name. May include path when creating file with base64 field. | [optional] 
 **path** | **str**| File path. Defaults to (/). | [optional] 

### Return type

[**ProjectFile**](ProjectFile.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: multipart/form-data, application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_project_files_delete**
> projects_project_files_delete(project, namespace, id)

Delete a project file

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
project = 'project_example' # str | Project unique identifer.
namespace = 'namespace_example' # str | User or team name.
id = 'id_example' # str | File unique identifier.

try: 
    # Delete a project file
    api_instance.projects_project_files_delete(project, namespace, id)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_project_files_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | **str**| Project unique identifer. | 
 **namespace** | **str**| User or team name. | 
 **id** | **str**| File unique identifier. | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_project_files_list**
> list[ProjectFile] projects_project_files_list(project, namespace, limit=limit, offset=offset, ordering=ordering, filename=filename, content=content)

Get project files

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
project = 'project_example' # str | Unique identifier for project file expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
limit = 'limit_example' # str | Limit when getting project file list. (optional)
offset = 'offset_example' # str | Offset when getting project file list. (optional)
ordering = 'ordering_example' # str | Ordering of list values when getting project file list. (optional)
filename = 'filename_example' # str | Exact file name, relative to the project root. If no such file is found, an empty list will be returned. (optional)
content = 'content_example' # str | Determines whether or not content is returned as base64. Defaults to false. (optional)

try: 
    # Get project files
    api_response = api_instance.projects_project_files_list(project, namespace, limit=limit, offset=offset, ordering=ordering, filename=filename, content=content)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_project_files_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | **str**| Unique identifier for project file expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **limit** | **str**| Limit when getting project file list. | [optional] 
 **offset** | **str**| Offset when getting project file list. | [optional] 
 **ordering** | **str**| Ordering of list values when getting project file list. | [optional] 
 **filename** | **str**| Exact file name, relative to the project root. If no such file is found, an empty list will be returned. | [optional] 
 **content** | **str**| Determines whether or not content is returned as base64. Defaults to false. | [optional] 

### Return type

[**list[ProjectFile]**](ProjectFile.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_project_files_read**
> ProjectFile projects_project_files_read(project, namespace, id, content=content)

Get a project file

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
project = 'project_example' # str | Project unique identifer.
namespace = 'namespace_example' # str | User or team name.
id = 'id_example' # str | File unique identifier.
content = 'content_example' # str | Determines whether or not content is returned as base64. Defaults to false. (optional)

try: 
    # Get a project file
    api_response = api_instance.projects_project_files_read(project, namespace, id, content=content)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_project_files_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | **str**| Project unique identifer. | 
 **namespace** | **str**| User or team name. | 
 **id** | **str**| File unique identifier. | 
 **content** | **str**| Determines whether or not content is returned as base64. Defaults to false. | [optional] 

### Return type

[**ProjectFile**](ProjectFile.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_project_files_replace**
> ProjectFile projects_project_files_replace(project, namespace, id, file=file, base64_data=base64_data, name=name, path=path)

Replace a project file

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
project = 'project_example' # str | Project unique identifer.
namespace = 'namespace_example' # str | User or team name.
id = 'id_example' # str | File unique identifier.
file = '/path/to/file.txt' # file | File to send, to create new file. This parameter is only used with form data and may include multiple files. (optional)
base64_data = 'base64_data_example' # str | Fila data, represented as base64. (optional)
name = 'name_example' # str | File name. May include path when creating file with base64 field. (optional)
path = 'path_example' # str | File path. Defaults to (/). (optional)

try: 
    # Replace a project file
    api_response = api_instance.projects_project_files_replace(project, namespace, id, file=file, base64_data=base64_data, name=name, path=path)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_project_files_replace: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | **str**| Project unique identifer. | 
 **namespace** | **str**| User or team name. | 
 **id** | **str**| File unique identifier. | 
 **file** | **file**| File to send, to create new file. This parameter is only used with form data and may include multiple files. | [optional] 
 **base64_data** | **str**| Fila data, represented as base64. | [optional] 
 **name** | **str**| File name. May include path when creating file with base64 field. | [optional] 
 **path** | **str**| File path. Defaults to (/). | [optional] 

### Return type

[**ProjectFile**](ProjectFile.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_project_files_update**
> ProjectFile projects_project_files_update(project, namespace, id, file=file, base64_data=base64_data, name=name, path=path)

Update a project file

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
project = 'project_example' # str | Project unique identifer.
namespace = 'namespace_example' # str | User or team name.
id = 'id_example' # str | File unique identifier.
file = '/path/to/file.txt' # file | File to send, to create new file. This parameter is only used with form data and may include multiple files. (optional)
base64_data = 'base64_data_example' # str | Fila data, represented as base64. (optional)
name = 'name_example' # str | File name. May include path when creating file with base64 field. (optional)
path = 'path_example' # str | File path. Defaults to (/). (optional)

try: 
    # Update a project file
    api_response = api_instance.projects_project_files_update(project, namespace, id, file=file, base64_data=base64_data, name=name, path=path)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_project_files_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | **str**| Project unique identifer. | 
 **namespace** | **str**| User or team name. | 
 **id** | **str**| File unique identifier. | 
 **file** | **file**| File to send, to create new file. This parameter is only used with form data and may include multiple files. | [optional] 
 **base64_data** | **str**| Fila data, represented as base64. | [optional] 
 **name** | **str**| File name. May include path when creating file with base64 field. | [optional] 
 **path** | **str**| File path. Defaults to (/). | [optional] 

### Return type

[**ProjectFile**](ProjectFile.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_read**
> Project projects_read(namespace, project)

Get a project

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
namespace = 'namespace_example' # str | User or team name.
project = 'project_example' # str | Project unique identifier expressed as UUID or name.

try: 
    # Get a project
    api_response = api_instance.projects_read(namespace, project)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **project** | **str**| Project unique identifier expressed as UUID or name. | 

### Return type

[**Project**](Project.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_replace**
> Project projects_replace(namespace, project, project_data=project_data)

Replace a project

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
namespace = 'namespace_example' # str | User or team namespace.
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
project_data = swagger_client.ProjectData() # ProjectData |  (optional)

try: 
    # Replace a project
    api_response = api_instance.projects_replace(namespace, project, project_data=project_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_replace: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team namespace. | 
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **project_data** | [**ProjectData**](ProjectData.md)|  | [optional] 

### Return type

[**Project**](Project.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_servers_api_key**
> JWT projects_servers_api_key(project, namespace, server)

Get server API key

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
server = 'server_example' # str | Server unique identifier expressed as UUID or name.

try: 
    # Get server API key
    api_response = api_instance.projects_servers_api_key(project, namespace, server)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_servers_api_key: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **server** | **str**| Server unique identifier expressed as UUID or name. | 

### Return type

[**JWT**](JWT.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_servers_auth**
> projects_servers_auth(project, namespace, server)

Server api key validation

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
server = 'server_example' # str | Server unique identifier expressed as UUID or name.

try: 
    # Server api key validation
    api_instance.projects_servers_auth(project, namespace, server)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_servers_auth: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **server** | **str**| Server unique identifier expressed as UUID or name. | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_servers_create**
> Server projects_servers_create(project, namespace, server_data=server_data)

Create a new server

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
project = 'project_example' # str | Project unique identifer expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
server_data = swagger_client.ServerData() # ServerData |  (optional)

try: 
    # Create a new server
    api_response = api_instance.projects_servers_create(project, namespace, server_data=server_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_servers_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | **str**| Project unique identifer expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **server_data** | [**ServerData**](ServerData.md)|  | [optional] 

### Return type

[**Server**](Server.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_servers_delete**
> projects_servers_delete(project, namespace, server)

Delete a server

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
project = 'project_example' # str | Project unique identifier.
namespace = 'namespace_example' # str | User or team name.
server = 'server_example' # str | User unique identifier.

try: 
    # Delete a server
    api_instance.projects_servers_delete(project, namespace, server)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_servers_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | **str**| Project unique identifier. | 
 **namespace** | **str**| User or team name. | 
 **server** | **str**| User unique identifier. | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_servers_list**
> list[Server] projects_servers_list(project, namespace, limit=limit, offset=offset, name=name, ordering=ordering)

Retrieve servers

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
limit = 'limit_example' # str | Limit results when getting server list. (optional)
offset = 'offset_example' # str | Offset results when getting server list. (optional)
name = 'name_example' # str | Server name. (optional)
ordering = 'ordering_example' # str | Ordering option when getting server list. (optional)

try: 
    # Retrieve servers
    api_response = api_instance.projects_servers_list(project, namespace, limit=limit, offset=offset, name=name, ordering=ordering)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_servers_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **limit** | **str**| Limit results when getting server list. | [optional] 
 **offset** | **str**| Offset results when getting server list. | [optional] 
 **name** | **str**| Server name. | [optional] 
 **ordering** | **str**| Ordering option when getting server list. | [optional] 

### Return type

[**list[Server]**](Server.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_servers_read**
> Server projects_servers_read(project, namespace, server)

Retrieve a server

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
server = 'server_example' # str | Server unique identifier expressed as UUID or name.

try: 
    # Retrieve a server
    api_response = api_instance.projects_servers_read(project, namespace, server)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_servers_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **server** | **str**| Server unique identifier expressed as UUID or name. | 

### Return type

[**Server**](Server.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_servers_replace**
> Server projects_servers_replace(project, namespace, server, server_data=server_data)

Replace a server

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
server = 'server_example' # str | Server unique identifier expressed as UUID or name.
server_data = swagger_client.ServerData() # ServerData |  (optional)

try: 
    # Replace a server
    api_response = api_instance.projects_servers_replace(project, namespace, server, server_data=server_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_servers_replace: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **server** | **str**| Server unique identifier expressed as UUID or name. | 
 **server_data** | [**ServerData**](ServerData.md)|  | [optional] 

### Return type

[**Server**](Server.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_servers_run_stats_create**
> ServerRunStatistics projects_servers_run_stats_create(server, project, namespace, serverrunstats_data=serverrunstats_data)

Create a new server's run statistics

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
server = 'server_example' # str | Server unique identifier expressed as UUID or name.
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
serverrunstats_data = swagger_client.ServerRunStatisticsData() # ServerRunStatisticsData |  (optional)

try: 
    # Create a new server's run statistics
    api_response = api_instance.projects_servers_run_stats_create(server, project, namespace, serverrunstats_data=serverrunstats_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_servers_run_stats_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **str**| Server unique identifier expressed as UUID or name. | 
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **serverrunstats_data** | [**ServerRunStatisticsData**](ServerRunStatisticsData.md)|  | [optional] 

### Return type

[**ServerRunStatistics**](ServerRunStatistics.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_servers_run_stats_delete**
> projects_servers_run_stats_delete(server, project, namespace, id)

Delete a server's statistics

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
server = 'server_example' # str | Server unique identifier expressed as UUID or name.
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
id = 'id_example' # str | Server run statistics unique identifier expressed as UUID.

try: 
    # Delete a server's statistics
    api_instance.projects_servers_run_stats_delete(server, project, namespace, id)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_servers_run_stats_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **str**| Server unique identifier expressed as UUID or name. | 
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **id** | **str**| Server run statistics unique identifier expressed as UUID. | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_servers_run_stats_read**
> ServerRunStatistics projects_servers_run_stats_read(server, project, namespace, id)

Retrieve statistics for a server

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
server = 'server_example' # str | Server unique identifier expressed as UUID or name.
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
id = 'id_example' # str | Run statistics unique identifier expressed as UUID.

try: 
    # Retrieve statistics for a server
    api_response = api_instance.projects_servers_run_stats_read(server, project, namespace, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_servers_run_stats_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **str**| Server unique identifier expressed as UUID or name. | 
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **id** | **str**| Run statistics unique identifier expressed as UUID. | 

### Return type

[**ServerRunStatistics**](ServerRunStatistics.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_servers_run_stats_replace**
> ServerRunStatistics projects_servers_run_stats_replace(server, project, namespace, id, serverrunstats_data=serverrunstats_data)

Replace a server's statistics

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
server = 'server_example' # str | Server unique identifier expressed as UUID or name.
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
id = 'id_example' # str | Server run statistics expressed as UUID.
serverrunstats_data = swagger_client.ServerRunStatisticsData() # ServerRunStatisticsData |  (optional)

try: 
    # Replace a server's statistics
    api_response = api_instance.projects_servers_run_stats_replace(server, project, namespace, id, serverrunstats_data=serverrunstats_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_servers_run_stats_replace: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **str**| Server unique identifier expressed as UUID or name. | 
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **id** | **str**| Server run statistics expressed as UUID. | 
 **serverrunstats_data** | [**ServerRunStatisticsData**](ServerRunStatisticsData.md)|  | [optional] 

### Return type

[**ServerRunStatistics**](ServerRunStatistics.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_servers_run_stats_update**
> ServerRunStatistics projects_servers_run_stats_update(server, project, namespace, id, serverrunstats_data=serverrunstats_data)

Update a server's statistics

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
server = 'server_example' # str | Server unique identifier expressed as UUID or name.
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
id = 'id_example' # str | Server run statistics unique identifier expressed as UUID.
serverrunstats_data = swagger_client.ServerRunStatisticsData() # ServerRunStatisticsData |  (optional)

try: 
    # Update a server's statistics
    api_response = api_instance.projects_servers_run_stats_update(server, project, namespace, id, serverrunstats_data=serverrunstats_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_servers_run_stats_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **str**| Server unique identifier expressed as UUID or name. | 
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **id** | **str**| Server run statistics unique identifier expressed as UUID. | 
 **serverrunstats_data** | [**ServerRunStatisticsData**](ServerRunStatisticsData.md)|  | [optional] 

### Return type

[**ServerRunStatistics**](ServerRunStatistics.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_servers_ssh_tunnels_create**
> SshTunnel projects_servers_ssh_tunnels_create(server, project, namespace, sshtunnel_data=sshtunnel_data)

Create SSH Tunnel associated to a server

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
server = 'server_example' # str | Server unique identifier expressed as UUID or name.
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
sshtunnel_data = swagger_client.SshTunnelData() # SshTunnelData |  (optional)

try: 
    # Create SSH Tunnel associated to a server
    api_response = api_instance.projects_servers_ssh_tunnels_create(server, project, namespace, sshtunnel_data=sshtunnel_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_servers_ssh_tunnels_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **str**| Server unique identifier expressed as UUID or name. | 
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **sshtunnel_data** | [**SshTunnelData**](SshTunnelData.md)|  | [optional] 

### Return type

[**SshTunnel**](SshTunnel.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_servers_ssh_tunnels_delete**
> projects_servers_ssh_tunnels_delete(server, project, namespace, tunnel)

Delete an SSH Tunnel associated to a server

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
server = 'server_example' # str | Server unique identifier expressed as UUID or name.
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
tunnel = 'tunnel_example' # str | SSH tunnel unique identifier expressed as UUID or name.

try: 
    # Delete an SSH Tunnel associated to a server
    api_instance.projects_servers_ssh_tunnels_delete(server, project, namespace, tunnel)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_servers_ssh_tunnels_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **str**| Server unique identifier expressed as UUID or name. | 
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **tunnel** | **str**| SSH tunnel unique identifier expressed as UUID or name. | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_servers_ssh_tunnels_list**
> list[SshTunnel] projects_servers_ssh_tunnels_list(server, project, namespace, limit=limit, offset=offset, ordering=ordering)

Get SSH Tunnels associated to a server

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
server = 'server_example' # str | Server unique identifier expressed as UUID or name.
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
limit = 'limit_example' # str | Limit retrieved items. (optional)
offset = 'offset_example' # str | Offset retrieved items. (optional)
ordering = 'ordering_example' # str | Order retrieved items. (optional)

try: 
    # Get SSH Tunnels associated to a server
    api_response = api_instance.projects_servers_ssh_tunnels_list(server, project, namespace, limit=limit, offset=offset, ordering=ordering)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_servers_ssh_tunnels_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **str**| Server unique identifier expressed as UUID or name. | 
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **limit** | **str**| Limit retrieved items. | [optional] 
 **offset** | **str**| Offset retrieved items. | [optional] 
 **ordering** | **str**| Order retrieved items. | [optional] 

### Return type

[**list[SshTunnel]**](SshTunnel.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_servers_ssh_tunnels_read**
> SshTunnel projects_servers_ssh_tunnels_read(server, project, namespace, tunnel)

Get an SSH Tunnel associated to a server

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
server = 'server_example' # str | Server unique identifier expressed as UUID or name.
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
tunnel = 'tunnel_example' # str | SSH tunnel unique identifier expressed as UUID or name.

try: 
    # Get an SSH Tunnel associated to a server
    api_response = api_instance.projects_servers_ssh_tunnels_read(server, project, namespace, tunnel)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_servers_ssh_tunnels_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **str**| Server unique identifier expressed as UUID or name. | 
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **tunnel** | **str**| SSH tunnel unique identifier expressed as UUID or name. | 

### Return type

[**SshTunnel**](SshTunnel.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_servers_ssh_tunnels_replace**
> SshTunnel projects_servers_ssh_tunnels_replace(server, project, namespace, tunnel, sshtunnel_data=sshtunnel_data)

Replace SSH Tunnel associated to a server

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
server = 'server_example' # str | Server unique identifier expressed as UUID or name.
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
tunnel = 'tunnel_example' # str | SSH tunnel unique identifier expressed as UUID or name.
sshtunnel_data = swagger_client.SshTunnelData() # SshTunnelData |  (optional)

try: 
    # Replace SSH Tunnel associated to a server
    api_response = api_instance.projects_servers_ssh_tunnels_replace(server, project, namespace, tunnel, sshtunnel_data=sshtunnel_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_servers_ssh_tunnels_replace: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **str**| Server unique identifier expressed as UUID or name. | 
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **tunnel** | **str**| SSH tunnel unique identifier expressed as UUID or name. | 
 **sshtunnel_data** | [**SshTunnelData**](SshTunnelData.md)|  | [optional] 

### Return type

[**SshTunnel**](SshTunnel.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_servers_ssh_tunnels_update**
> SshTunnel projects_servers_ssh_tunnels_update(server, project, namespace, tunnel, sshtunnel_data=sshtunnel_data)

Update an SSH Tunnel associated to a server

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
server = 'server_example' # str | 
project = 'project_example' # str | 
namespace = 'namespace_example' # str | User or team name.
tunnel = 'tunnel_example' # str | 
sshtunnel_data = swagger_client.SshTunnelData() # SshTunnelData |  (optional)

try: 
    # Update an SSH Tunnel associated to a server
    api_response = api_instance.projects_servers_ssh_tunnels_update(server, project, namespace, tunnel, sshtunnel_data=sshtunnel_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_servers_ssh_tunnels_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **str**|  | 
 **project** | **str**|  | 
 **namespace** | **str**| User or team name. | 
 **tunnel** | **str**|  | 
 **sshtunnel_data** | [**SshTunnelData**](SshTunnelData.md)|  | [optional] 

### Return type

[**SshTunnel**](SshTunnel.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_servers_start**
> projects_servers_start(project, namespace, server)

Start a server

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
server = 'server_example' # str | Server unique identifier expressed as UUID or name.

try: 
    # Start a server
    api_instance.projects_servers_start(project, namespace, server)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_servers_start: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **server** | **str**| Server unique identifier expressed as UUID or name. | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_servers_stats_delete**
> projects_servers_stats_delete(server, project, namespace, id)

Delete a server's statistics

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
server = 'server_example' # str | Server unique identifier expressed as UUID or name.
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
id = 'id_example' # str | Stats unique identifier expressed as UUID.

try: 
    # Delete a server's statistics
    api_instance.projects_servers_stats_delete(server, project, namespace, id)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_servers_stats_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **str**| Server unique identifier expressed as UUID or name. | 
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **id** | **str**| Stats unique identifier expressed as UUID. | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_servers_stats_read**
> ServerStatistics projects_servers_stats_read(server, project, namespace, id)

Retrieve a server's statistics

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
server = 'server_example' # str | Server unique identifier expressed as UUID or name.
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
id = 'id_example' # str | Server statistics unique identifier expressed as UUID.

try: 
    # Retrieve a server's statistics
    api_response = api_instance.projects_servers_stats_read(server, project, namespace, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_servers_stats_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **str**| Server unique identifier expressed as UUID or name. | 
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **id** | **str**| Server statistics unique identifier expressed as UUID. | 

### Return type

[**ServerStatistics**](ServerStatistics.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_servers_stats_replace**
> ServerStatistics projects_servers_stats_replace(server, project, namespace, id, serverstats_data=serverstats_data)

Replace a server's statistics

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
server = 'server_example' # str | Server unique identifier expressed as UUID or name.
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
id = 'id_example' # str | Server statistics unique identifier expressed as UUID.
serverstats_data = swagger_client.ServerStatisticsData() # ServerStatisticsData |  (optional)

try: 
    # Replace a server's statistics
    api_response = api_instance.projects_servers_stats_replace(server, project, namespace, id, serverstats_data=serverstats_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_servers_stats_replace: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **str**| Server unique identifier expressed as UUID or name. | 
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **id** | **str**| Server statistics unique identifier expressed as UUID. | 
 **serverstats_data** | [**ServerStatisticsData**](ServerStatisticsData.md)|  | [optional] 

### Return type

[**ServerStatistics**](ServerStatistics.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_servers_stats_update**
> ServerStatistics projects_servers_stats_update(server, project, namespace, id, serverstats_data=serverstats_data)

Update a server's statistics

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
server = 'server_example' # str | Server unique identifier expressed as UUID or name.
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
id = 'id_example' # str | Server statistics unique identifier expressed as UUID.
serverstats_data = swagger_client.ServerStatisticsData() # ServerStatisticsData |  (optional)

try: 
    # Update a server's statistics
    api_response = api_instance.projects_servers_stats_update(server, project, namespace, id, serverstats_data=serverstats_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_servers_stats_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **str**| Server unique identifier expressed as UUID or name. | 
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **id** | **str**| Server statistics unique identifier expressed as UUID. | 
 **serverstats_data** | [**ServerStatisticsData**](ServerStatisticsData.md)|  | [optional] 

### Return type

[**ServerStatistics**](ServerStatistics.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_servers_stop**
> projects_servers_stop(project, namespace, server)

Stop a server

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
server = 'server_example' # str | Server unique identifier expressed as UUID or name.

try: 
    # Stop a server
    api_instance.projects_servers_stop(project, namespace, server)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_servers_stop: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **server** | **str**| Server unique identifier expressed as UUID or name. | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_servers_update**
> Server projects_servers_update(project, namespace, server, server_data=server_data)

Update a server

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
server = 'server_example' # str | Server unique identifier expressed as UUID or name.
server_data = swagger_client.ServerData() # ServerData |  (optional)

try: 
    # Update a server
    api_response = api_instance.projects_servers_update(project, namespace, server, server_data=server_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_servers_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **server** | **str**| Server unique identifier expressed as UUID or name. | 
 **server_data** | [**ServerData**](ServerData.md)|  | [optional] 

### Return type

[**Server**](Server.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **projects_update**
> Project projects_update(namespace, project, project_data=project_data)

Update a project

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
namespace = 'namespace_example' # str | User or team name.
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
project_data = swagger_client.ProjectData() # ProjectData |  (optional)

try: 
    # Update a project
    api_response = api_instance.projects_update(namespace, project, project_data=project_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->projects_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **project_data** | [**ProjectData**](ProjectData.md)|  | [optional] 

### Return type

[**Project**](Project.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **service_trigger_create**
> ServerAction service_trigger_create(server, project, namespace, server_action=server_action)

Create a new server trigger

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
server = 'server_example' # str | Server unique identifier expressed as UUID or name.
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
server_action = swagger_client.ServerActionData() # ServerActionData | Server action. (optional)

try: 
    # Create a new server trigger
    api_response = api_instance.service_trigger_create(server, project, namespace, server_action=server_action)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->service_trigger_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **str**| Server unique identifier expressed as UUID or name. | 
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **server_action** | [**ServerActionData**](ServerActionData.md)| Server action. | [optional] 

### Return type

[**ServerAction**](ServerAction.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **service_trigger_delete**
> service_trigger_delete(server, project, namespace, trigger)

Delete a server trigger

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
server = 'server_example' # str | Server unique identifier expressed as UUID or name.
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
trigger = 'trigger_example' # str | Trigger identifier expressed as UUID or name.

try: 
    # Delete a server trigger
    api_instance.service_trigger_delete(server, project, namespace, trigger)
except ApiException as e:
    print("Exception when calling ProjectsApi->service_trigger_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **str**| Server unique identifier expressed as UUID or name. | 
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **trigger** | **str**| Trigger identifier expressed as UUID or name. | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **service_trigger_list**
> list[ServerAction] service_trigger_list(server, project, namespace, name=name, limit=limit, offset=offset, ordering=ordering)

Retrieve server triggers

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
server = 'server_example' # str | Server unique identifier expressed as UUID or name.
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
name = 'name_example' # str | Trigger name. (optional)
limit = 'limit_example' # str | Limit when getting triggers. (optional)
offset = 'offset_example' # str | Offset when getting triggers. (optional)
ordering = 'ordering_example' # str | Ordering when getting triggers. (optional)

try: 
    # Retrieve server triggers
    api_response = api_instance.service_trigger_list(server, project, namespace, name=name, limit=limit, offset=offset, ordering=ordering)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->service_trigger_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **str**| Server unique identifier expressed as UUID or name. | 
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **name** | **str**| Trigger name. | [optional] 
 **limit** | **str**| Limit when getting triggers. | [optional] 
 **offset** | **str**| Offset when getting triggers. | [optional] 
 **ordering** | **str**| Ordering when getting triggers. | [optional] 

### Return type

[**list[ServerAction]**](ServerAction.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **service_trigger_read**
> ServerAction service_trigger_read(server, project, namespace, trigger)

Get a server trigger

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
server = 'server_example' # str | Server unique identifier expressed as UUID or name.
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
trigger = 'trigger_example' # str | Trigger unique identifier.

try: 
    # Get a server trigger
    api_response = api_instance.service_trigger_read(server, project, namespace, trigger)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->service_trigger_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **str**| Server unique identifier expressed as UUID or name. | 
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **trigger** | **str**| Trigger unique identifier. | 

### Return type

[**ServerAction**](ServerAction.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **service_trigger_replace**
> ServerAction service_trigger_replace(server, project, namespace, trigger, server_action=server_action)

Replace a server trigger

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
server = 'server_example' # str | Server unique identifier expressed as UUID or name.
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
trigger = 'trigger_example' # str | Trigger unique identifier.
server_action = swagger_client.ServerActionData() # ServerActionData |  (optional)

try: 
    # Replace a server trigger
    api_response = api_instance.service_trigger_replace(server, project, namespace, trigger, server_action=server_action)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->service_trigger_replace: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **str**| Server unique identifier expressed as UUID or name. | 
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **trigger** | **str**| Trigger unique identifier. | 
 **server_action** | [**ServerActionData**](ServerActionData.md)|  | [optional] 

### Return type

[**ServerAction**](ServerAction.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **service_trigger_update**
> ServerAction service_trigger_update(server, project, namespace, trigger, server_action=server_action)

Update a server trigger

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ProjectsApi()
server = 'server_example' # str | Server unique identifier expressed as UUID or name.
project = 'project_example' # str | Project unique identifier expressed as UUID or name.
namespace = 'namespace_example' # str | User or team name.
trigger = 'trigger_example' # str | Trigger identifier expressed as UUID or name.
server_action = swagger_client.ServerActionData() # ServerActionData |  (optional)

try: 
    # Update a server trigger
    api_response = api_instance.service_trigger_update(server, project, namespace, trigger, server_action=server_action)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectsApi->service_trigger_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server** | **str**| Server unique identifier expressed as UUID or name. | 
 **project** | **str**| Project unique identifier expressed as UUID or name. | 
 **namespace** | **str**| User or team name. | 
 **trigger** | **str**| Trigger identifier expressed as UUID or name. | 
 **server_action** | [**ServerActionData**](ServerActionData.md)|  | [optional] 

### Return type

[**ServerAction**](ServerAction.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

