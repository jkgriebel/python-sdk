# Collaborator

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Collaborator unique identifier in UUID format. | [optional] 
**owner** | **bool** | Boolean that states whether collaborator is project owner, or not.  | [optional] 
**user** | **str** | Collaborator user name. | [optional] 
**project** | **str** | Collaborator project name. | [optional] 
**joined** | **str** | Date time of when collaborator joined. | [optional] 
**username** | **str** | Collaborator&#39;s user name. This must be a valid user name within the system.  | [optional] 
**email** | **str** | Collaborator&#39;s valid email address. | [optional] 
**first_name** | **str** | Collaborator&#39;s first name. | [optional] 
**last_name** | **str** | Collaborator&#39;s last name. | [optional] 
**permissions** | **list[str]** | Collaborator permissions. Project creators are assigned owner priviledges by default. Permissions are write and read.  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


