# ServerConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | Server type. | [optional] 
**script** | **str** | Server script file. | [optional] 
**function** | **str** | Server function. | [optional] 
**command** | **str** | Command to run | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


