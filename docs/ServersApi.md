# swagger_client.ServersApi

All URIs are relative to *https://api.3blades.ai*

Method | HTTP request | Description
------------- | ------------- | -------------
[**servers_options_resources_read**](ServersApi.md#servers_options_resources_read) | **GET** /v1/servers/options/server-size/{size}/ | Get a server size by id
[**servers_options_server_size_create**](ServersApi.md#servers_options_server_size_create) | **POST** /v1/servers/options/server-size/ | Create a new server size item
[**servers_options_server_size_delete**](ServersApi.md#servers_options_server_size_delete) | **DELETE** /v1/servers/options/server-size/{size}/ | Delete a server size by id
[**servers_options_server_size_replace**](ServersApi.md#servers_options_server_size_replace) | **PUT** /v1/servers/options/server-size/{size}/ | Replace a server size by id
[**servers_options_server_size_update**](ServersApi.md#servers_options_server_size_update) | **PATCH** /v1/servers/options/server-size/{size}/ | Update a server size by id
[**servers_options_sizes_list**](ServersApi.md#servers_options_sizes_list) | **GET** /v1/servers/options/server-size/ | Retrieve available server sizes


# **servers_options_resources_read**
> ServerSize servers_options_resources_read(size)

Get a server size by id

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ServersApi()
size = 'size_example' # str | Server size unique identifier expressed as UUID or name.

try: 
    # Get a server size by id
    api_response = api_instance.servers_options_resources_read(size)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServersApi->servers_options_resources_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **str**| Server size unique identifier expressed as UUID or name. | 

### Return type

[**ServerSize**](ServerSize.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **servers_options_server_size_create**
> ServerSize servers_options_server_size_create(serversize_data=serversize_data)

Create a new server size item

Only super users with on-premises version have acceess to this endpoint.

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ServersApi()
serversize_data = swagger_client.ServerSizeData() # ServerSizeData |  (optional)

try: 
    # Create a new server size item
    api_response = api_instance.servers_options_server_size_create(serversize_data=serversize_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServersApi->servers_options_server_size_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serversize_data** | [**ServerSizeData**](ServerSizeData.md)|  | [optional] 

### Return type

[**ServerSize**](ServerSize.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **servers_options_server_size_delete**
> servers_options_server_size_delete(size)

Delete a server size by id

Only super users with on-premises version have acceess to this endpoint.

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ServersApi()
size = 'size_example' # str | Server size unique identifier expressed as UUID or name.

try: 
    # Delete a server size by id
    api_instance.servers_options_server_size_delete(size)
except ApiException as e:
    print("Exception when calling ServersApi->servers_options_server_size_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **str**| Server size unique identifier expressed as UUID or name. | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **servers_options_server_size_replace**
> ServerSize servers_options_server_size_replace(size, serversize_data=serversize_data)

Replace a server size by id

Only super users with on-premises version have acceess to this endpoint.

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ServersApi()
size = 'size_example' # str | Server size unique identifier expressed as UUID or name.
serversize_data = swagger_client.ServerSizeData() # ServerSizeData |  (optional)

try: 
    # Replace a server size by id
    api_response = api_instance.servers_options_server_size_replace(size, serversize_data=serversize_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServersApi->servers_options_server_size_replace: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **str**| Server size unique identifier expressed as UUID or name. | 
 **serversize_data** | [**ServerSizeData**](ServerSizeData.md)|  | [optional] 

### Return type

[**ServerSize**](ServerSize.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **servers_options_server_size_update**
> ServerSize servers_options_server_size_update(size, serversize_data=serversize_data)

Update a server size by id

Only super users with on-premises version have acceess to this endpoint.

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ServersApi()
size = 'size_example' # str | Server size unique identifier expressed as UUID or name.
serversize_data = swagger_client.ServerSizeData() # ServerSizeData |  (optional)

try: 
    # Update a server size by id
    api_response = api_instance.servers_options_server_size_update(size, serversize_data=serversize_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServersApi->servers_options_server_size_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **str**| Server size unique identifier expressed as UUID or name. | 
 **serversize_data** | [**ServerSizeData**](ServerSizeData.md)|  | [optional] 

### Return type

[**ServerSize**](ServerSize.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **servers_options_sizes_list**
> list[ServerSize] servers_options_sizes_list(limit=limit, offset=offset, ordering=ordering)

Retrieve available server sizes

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.ServersApi()
limit = 'limit_example' # str | Set limit when retrieving items. (optional)
offset = 'offset_example' # str | Offset when retrieving items. (optional)
ordering = 'ordering_example' # str | Set order when retrieving items. (optional)

try: 
    # Retrieve available server sizes
    api_response = api_instance.servers_options_sizes_list(limit=limit, offset=offset, ordering=ordering)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ServersApi->servers_options_sizes_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **str**| Set limit when retrieving items. | [optional] 
 **offset** | **str**| Offset when retrieving items. | [optional] 
 **ordering** | **str**| Set order when retrieving items. | [optional] 

### Return type

[**list[ServerSize]**](ServerSize.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

