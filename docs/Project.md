# Project

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Unique identifier for project as UUID. | [optional] 
**name** | **str** | Project name. | 
**description** | **str** | Project description. | [optional] 
**team** | **str** | Project team name. | [optional] 
**private** | **bool** | Value that states whether project is private or public. | [optional] 
**last_updated** | **str** | Date and time when project was last updated. | [optional] 
**owner** | **str** | Username of project owner. | [optional] 
**collaborators** | **list[str]** | Array of project collaborators. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


