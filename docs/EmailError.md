# EmailError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**non_field_errors** | **list[str]** | Errors not connected to any field. | [optional] 
**address** | **list[str]** | Address field errors. | [optional] 
**public** | **list[str]** | Public field errors. | [optional] 
**unsubscribed** | **list[str]** | Unsubscribed field errors. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


