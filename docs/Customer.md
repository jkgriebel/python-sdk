# Customer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Customer unique identifier expressed as UUID. | [optional] 
**stripe_id** | **str** | Stripe identifier. | [optional] 
**created** | **str** | Date and time for customer creation. | [optional] 
**metadata** | **object** | Optional customer meta data object. | [optional] 
**livemode** | **bool** | Customer live mode. | [optional] 
**account_balance** | **int** | Customer account balance. | [optional] 
**currency** | **str** | Currency used by customer. | [optional] 
**last_invoice_sync** | **str** | Date and time for last invoice sync. | [optional] 
**user** | **str** | Customer primary user. | 
**default_source** | **str** | Customer default source. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


