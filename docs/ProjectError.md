# ProjectError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**non_field_errors** | **list[str]** | Errors not connected to any field. | [optional] 
**id** | **list[str]** | Id field errors. | [optional] 
**name** | **list[str]** | Name field errors. | [optional] 
**description** | **list[str]** | Description field errors. | [optional] 
**private** | **list[str]** | Private field errors. | [optional] 
**last_updated** | **list[str]** | Last_updated field errors. | [optional] 
**owner** | **list[str]** | Owner field errors. | [optional] 
**collaborators** | **list[str]** | Collaborators field errors. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


