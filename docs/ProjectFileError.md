# ProjectFileError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**non_field_errors** | **list[str]** | Errors not connected to any field | [optional] 
**id** | **list[str]** | id field errors | [optional] 
**project** | **list[str]** | project field errors | [optional] 
**file** | **list[str]** | file field errors | [optional] 
**content** | **list[str]** | base64_data field errors | [optional] 
**name** | **list[str]** | name field errors | [optional] 
**path** | **list[str]** | path field errors | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


