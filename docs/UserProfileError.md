# UserProfileError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**non_field_errors** | **list[str]** | Errors not connected to any field. | [optional] 
**bio** | **list[str]** | Bio field errors. | [optional] 
**url** | **list[str]** | URL field errors. | [optional] 
**location** | **list[str]** | Location field errors. | [optional] 
**company** | **list[str]** | Company field errors. | [optional] 
**timezone** | **list[str]** | Timezone field errors. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


