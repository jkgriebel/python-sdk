# SshTunnelError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**non_field_errors** | **list[str]** | Errors not connected to any field. | [optional] 
**id** | **list[str]** | id field errors. | [optional] 
**name** | **list[str]** | name field errors. | [optional] 
**host** | **list[str]** | host field errors. | [optional] 
**local_port** | **list[str]** | local_port field errors. | [optional] 
**remote_port** | **list[str]** | remote_port field errors. | [optional] 
**endpoint** | **list[str]** | endpoint field errors. | [optional] 
**username** | **list[str]** | username field errors. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


