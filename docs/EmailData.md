# EmailData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **str** | Email address. | 
**public** | **bool** | Public or private email address. | [optional] 
**unsubscribed** | **bool** | Unsubscribed or suscribed. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


