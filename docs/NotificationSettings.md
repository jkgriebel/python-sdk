# NotificationSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Notification unique identifier expressed as a UUID | [optional] 
**user** | **str** | User the notification settings is for. | [optional] 
**entity** | **str** | The entity type that the settings pertain to. | [optional] 
**enabled** | **bool** | Turn notifications on or off entirely. | 
**emails_enabled** | **bool** | Turn emails on or off. | 
**email_address** | **str** | Email address to send notifications. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


