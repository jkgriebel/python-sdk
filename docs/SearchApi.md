# swagger_client.SearchApi

All URIs are relative to *https://api.3blades.ai*

Method | HTTP request | Description
------------- | ------------- | -------------
[**search**](SearchApi.md#search) | **GET** /v1/{namespace}/search/ | Get a search results


# **search**
> list[Search] search(namespace, q, type=type, limit=limit, offset=offset)

Get a search results

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.SearchApi()
namespace = 'namespace_example' # str | User or team name.
q = 'q_example' # str | Search string.
type = 'type_example' # str | Limit results to specific types. (optional)
limit = 'limit_example' # str | Limit data when getting items. (optional)
offset = 'offset_example' # str | Offset data when getting items. (optional)

try: 
    # Get a search results
    api_response = api_instance.search(namespace, q, type=type, limit=limit, offset=offset)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SearchApi->search: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **q** | **str**| Search string. | 
 **type** | **str**| Limit results to specific types. | [optional] 
 **limit** | **str**| Limit data when getting items. | [optional] 
 **offset** | **str**| Offset data when getting items. | [optional] 

### Return type

[**list[Search]**](Search.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

