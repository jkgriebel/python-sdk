# Notification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Notification unique identifier expressed as a UUID | [optional] 
**user** | **str** | User the notification is for. | [optional] 
**actor** | **str** | Unique identifier of the object that triggered the notification. | [optional] 
**target** | **str** | Unique identifier of the object that was acted upon by the object referred to in actor. | [optional] 
**type** | **str** | Type of the notification, e.g. invoice.created, subscription.created, etc. | [optional] 
**timestamp** | **str** | Timestamp that the notification was created at. | [optional] 
**read** | **bool** | Whether or not the notification has been marked as read by the user. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


