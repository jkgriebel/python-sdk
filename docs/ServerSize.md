# ServerSize

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Server size unique identifier. | [optional] 
**name** | **str** | Server size name. | 
**cpu** | **int** | CPU availability, equivalent to --cpus&#x3D;&lt;value&gt;. For example if a host has 2 CPU&#39;s, then a value of 1.5 would be limit the container to using all of the first CPU and half of the second CPU.  | 
**memory** | **int** | Memory restriction for container. | 
**active** | **bool** | States whether the Server size is active, or not. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


