# swagger_client.BillingApi

All URIs are relative to *https://api.3blades.ai*

Method | HTTP request | Description
------------- | ------------- | -------------
[**billing_cards_create**](BillingApi.md#billing_cards_create) | **POST** /v1/{namespace}/billing/cards/ | Create new credit card
[**billing_cards_delete**](BillingApi.md#billing_cards_delete) | **DELETE** /v1/{namespace}/billing/cards/{id}/ | Delete a credit card
[**billing_cards_list**](BillingApi.md#billing_cards_list) | **GET** /v1/{namespace}/billing/cards/ | Get credit cards
[**billing_cards_read**](BillingApi.md#billing_cards_read) | **GET** /v1/{namespace}/billing/cards/{id}/ | Get credit card by id
[**billing_cards_replace**](BillingApi.md#billing_cards_replace) | **PUT** /v1/{namespace}/billing/cards/{id}/ | Replace a credit card
[**billing_cards_update**](BillingApi.md#billing_cards_update) | **PATCH** /v1/{namespace}/billing/cards/{id}/ | Update a credit card
[**billing_invoice_items_list**](BillingApi.md#billing_invoice_items_list) | **GET** /v1/{namespace}/billing/invoices/{invoice_id}/invoice-items/ | Get invoice items for a given invoice.
[**billing_invoice_items_read**](BillingApi.md#billing_invoice_items_read) | **GET** /v1/{namespace}/billing/invoices/{invoice_id}/invoice-items/{id} | Get a specific InvoiceItem.
[**billing_invoices_list**](BillingApi.md#billing_invoices_list) | **GET** /v1/{namespace}/billing/invoices/ | Get invoices
[**billing_invoices_read**](BillingApi.md#billing_invoices_read) | **GET** /v1/{namespace}/billing/invoices/{id}/ | Get an invoice
[**billing_plans_list**](BillingApi.md#billing_plans_list) | **GET** /v1/{namespace}/billing/plans/ | Get billing plans
[**billing_plans_read**](BillingApi.md#billing_plans_read) | **GET** /v1/{namespace}/billing/plans/{id}/ | Get a billing plan
[**billing_subscriptions_create**](BillingApi.md#billing_subscriptions_create) | **POST** /v1/{namespace}/billing/subscriptions/ | Create a new subscription
[**billing_subscriptions_delete**](BillingApi.md#billing_subscriptions_delete) | **DELETE** /v1/{namespace}/billing/subscriptions/{id}/ | Delete a subscription
[**billing_subscriptions_list**](BillingApi.md#billing_subscriptions_list) | **GET** /v1/{namespace}/billing/subscriptions/ | Get active subscriptons
[**billing_subscriptions_read**](BillingApi.md#billing_subscriptions_read) | **GET** /v1/{namespace}/billing/subscriptions/{id}/ | Get a subscriptions


# **billing_cards_create**
> Card billing_cards_create(namespace, card_data=card_data)

Create new credit card

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.BillingApi()
namespace = 'namespace_example' # str | User or team name.
card_data = swagger_client.CardDataPost() # CardDataPost |  (optional)

try: 
    # Create new credit card
    api_response = api_instance.billing_cards_create(namespace, card_data=card_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BillingApi->billing_cards_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **card_data** | [**CardDataPost**](CardDataPost.md)|  | [optional] 

### Return type

[**Card**](Card.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **billing_cards_delete**
> billing_cards_delete(namespace, id)

Delete a credit card

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.BillingApi()
namespace = 'namespace_example' # str | User or team name.
id = 'id_example' # str | Card unique identifier expressed as UUID.

try: 
    # Delete a credit card
    api_instance.billing_cards_delete(namespace, id)
except ApiException as e:
    print("Exception when calling BillingApi->billing_cards_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **id** | **str**| Card unique identifier expressed as UUID. | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **billing_cards_list**
> list[Card] billing_cards_list(namespace, limit=limit, offset=offset, ordering=ordering)

Get credit cards

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.BillingApi()
namespace = 'namespace_example' # str | User or team name.
limit = 'limit_example' # str | Set limit when retrieving credit or debit cards. (optional)
offset = 'offset_example' # str | Set offset when retriving cards. (optional)
ordering = 'ordering_example' # str | Order when retrieving cards. (optional)

try: 
    # Get credit cards
    api_response = api_instance.billing_cards_list(namespace, limit=limit, offset=offset, ordering=ordering)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BillingApi->billing_cards_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **limit** | **str**| Set limit when retrieving credit or debit cards. | [optional] 
 **offset** | **str**| Set offset when retriving cards. | [optional] 
 **ordering** | **str**| Order when retrieving cards. | [optional] 

### Return type

[**list[Card]**](Card.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **billing_cards_read**
> Card billing_cards_read(namespace, id)

Get credit card by id

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.BillingApi()
namespace = 'namespace_example' # str | User or team name.
id = 'id_example' # str | User unique identifier expressed as UUID.

try: 
    # Get credit card by id
    api_response = api_instance.billing_cards_read(namespace, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BillingApi->billing_cards_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **id** | **str**| User unique identifier expressed as UUID. | 

### Return type

[**Card**](Card.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **billing_cards_replace**
> Card billing_cards_replace(namespace, id, card_data=card_data)

Replace a credit card

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.BillingApi()
namespace = 'namespace_example' # str | User or team name.
id = 'id_example' # str | 
card_data = swagger_client.CardDataPutandPatch() # CardDataPutandPatch |  (optional)

try: 
    # Replace a credit card
    api_response = api_instance.billing_cards_replace(namespace, id, card_data=card_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BillingApi->billing_cards_replace: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **id** | **str**|  | 
 **card_data** | [**CardDataPutandPatch**](CardDataPutandPatch.md)|  | [optional] 

### Return type

[**Card**](Card.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **billing_cards_update**
> Card billing_cards_update(namespace, id, card_data=card_data)

Update a credit card

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.BillingApi()
namespace = 'namespace_example' # str | User or team name.
id = 'id_example' # str | Card unique identifier.
card_data = swagger_client.CardDataPutandPatch() # CardDataPutandPatch |  (optional)

try: 
    # Update a credit card
    api_response = api_instance.billing_cards_update(namespace, id, card_data=card_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BillingApi->billing_cards_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **id** | **str**| Card unique identifier. | 
 **card_data** | [**CardDataPutandPatch**](CardDataPutandPatch.md)|  | [optional] 

### Return type

[**Card**](Card.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **billing_invoice_items_list**
> list[InvoiceItem] billing_invoice_items_list(namespace, invoice_id, limit=limit, offset=offset, ordering=ordering)

Get invoice items for a given invoice.

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.BillingApi()
namespace = 'namespace_example' # str | User or team name.
invoice_id = 'invoice_id_example' # str | Invoice id, expressed as UUID.
limit = 'limit_example' # str | Limit when getting items. (optional)
offset = 'offset_example' # str | Offset when getting items. (optional)
ordering = 'ordering_example' # str | Ordering when getting items. (optional)

try: 
    # Get invoice items for a given invoice.
    api_response = api_instance.billing_invoice_items_list(namespace, invoice_id, limit=limit, offset=offset, ordering=ordering)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BillingApi->billing_invoice_items_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **invoice_id** | **str**| Invoice id, expressed as UUID. | 
 **limit** | **str**| Limit when getting items. | [optional] 
 **offset** | **str**| Offset when getting items. | [optional] 
 **ordering** | **str**| Ordering when getting items. | [optional] 

### Return type

[**list[InvoiceItem]**](InvoiceItem.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **billing_invoice_items_read**
> InvoiceItem billing_invoice_items_read(namespace, invoice_id, id)

Get a specific InvoiceItem.

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.BillingApi()
namespace = 'namespace_example' # str | User or team name.
invoice_id = 'invoice_id_example' # str | Invoice id, expressed as UUID.
id = 'id_example' # str | InvoiceItem id, expressed as UUID.

try: 
    # Get a specific InvoiceItem.
    api_response = api_instance.billing_invoice_items_read(namespace, invoice_id, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BillingApi->billing_invoice_items_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **invoice_id** | **str**| Invoice id, expressed as UUID. | 
 **id** | **str**| InvoiceItem id, expressed as UUID. | 

### Return type

[**InvoiceItem**](InvoiceItem.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **billing_invoices_list**
> list[Invoice] billing_invoices_list(namespace, limit=limit, offset=offset, ordering=ordering)

Get invoices

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.BillingApi()
namespace = 'namespace_example' # str | User or team name.
limit = 'limit_example' # str | Limit when getting items. (optional)
offset = 'offset_example' # str | Offset when getting items. (optional)
ordering = 'ordering_example' # str | Ordering when getting items. (optional)

try: 
    # Get invoices
    api_response = api_instance.billing_invoices_list(namespace, limit=limit, offset=offset, ordering=ordering)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BillingApi->billing_invoices_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **limit** | **str**| Limit when getting items. | [optional] 
 **offset** | **str**| Offset when getting items. | [optional] 
 **ordering** | **str**| Ordering when getting items. | [optional] 

### Return type

[**list[Invoice]**](Invoice.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **billing_invoices_read**
> Invoice billing_invoices_read(namespace, id)

Get an invoice

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.BillingApi()
namespace = 'namespace_example' # str | User or team name.
id = 'id_example' # str | Invoice unique identifier expressed as UUID.

try: 
    # Get an invoice
    api_response = api_instance.billing_invoices_read(namespace, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BillingApi->billing_invoices_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **id** | **str**| Invoice unique identifier expressed as UUID. | 

### Return type

[**Invoice**](Invoice.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **billing_plans_list**
> list[Plan] billing_plans_list(namespace, limit=limit, offset=offset, ordering=ordering)

Get billing plans

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.BillingApi()
namespace = 'namespace_example' # str | User or team name.
limit = 'limit_example' # str | Limit when getting items. (optional)
offset = 'offset_example' # str | Offset when getting items. (optional)
ordering = 'ordering_example' # str | Ordering when getting items. (optional)

try: 
    # Get billing plans
    api_response = api_instance.billing_plans_list(namespace, limit=limit, offset=offset, ordering=ordering)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BillingApi->billing_plans_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **limit** | **str**| Limit when getting items. | [optional] 
 **offset** | **str**| Offset when getting items. | [optional] 
 **ordering** | **str**| Ordering when getting items. | [optional] 

### Return type

[**list[Plan]**](Plan.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **billing_plans_read**
> Plan billing_plans_read(namespace, id)

Get a billing plan

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.BillingApi()
namespace = 'namespace_example' # str | User or team name.
id = 'id_example' # str | Plan unique identifier expressed as UUID.

try: 
    # Get a billing plan
    api_response = api_instance.billing_plans_read(namespace, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BillingApi->billing_plans_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **id** | **str**| Plan unique identifier expressed as UUID. | 

### Return type

[**Plan**](Plan.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **billing_subscriptions_create**
> Subscription billing_subscriptions_create(namespace, subscription_data=subscription_data)

Create a new subscription

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.BillingApi()
namespace = 'namespace_example' # str | User or team name.
subscription_data = swagger_client.SubscriptionData() # SubscriptionData |  (optional)

try: 
    # Create a new subscription
    api_response = api_instance.billing_subscriptions_create(namespace, subscription_data=subscription_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BillingApi->billing_subscriptions_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **subscription_data** | [**SubscriptionData**](SubscriptionData.md)|  | [optional] 

### Return type

[**Subscription**](Subscription.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **billing_subscriptions_delete**
> billing_subscriptions_delete(namespace, id)

Delete a subscription

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.BillingApi()
namespace = 'namespace_example' # str | User or team name.
id = 'id_example' # str | Subscription unique identifier expressed as UUID.

try: 
    # Delete a subscription
    api_instance.billing_subscriptions_delete(namespace, id)
except ApiException as e:
    print("Exception when calling BillingApi->billing_subscriptions_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **id** | **str**| Subscription unique identifier expressed as UUID. | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **billing_subscriptions_list**
> list[Subscription] billing_subscriptions_list(namespace, limit=limit, offset=offset, ordering=ordering)

Get active subscriptons

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.BillingApi()
namespace = 'namespace_example' # str | User or team name.
limit = 'limit_example' # str | Limit when getting items. (optional)
offset = 'offset_example' # str | Offset when getting items. (optional)
ordering = 'ordering_example' # str | Ordering when getting items. (optional)

try: 
    # Get active subscriptons
    api_response = api_instance.billing_subscriptions_list(namespace, limit=limit, offset=offset, ordering=ordering)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BillingApi->billing_subscriptions_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **limit** | **str**| Limit when getting items. | [optional] 
 **offset** | **str**| Offset when getting items. | [optional] 
 **ordering** | **str**| Ordering when getting items. | [optional] 

### Return type

[**list[Subscription]**](Subscription.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **billing_subscriptions_read**
> Subscription billing_subscriptions_read(namespace, id)

Get a subscriptions

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.BillingApi()
namespace = 'namespace_example' # str | User or team name.
id = 'id_example' # str | Unique identifier expressed as UUID.

try: 
    # Get a subscriptions
    api_response = api_instance.billing_subscriptions_read(namespace, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BillingApi->billing_subscriptions_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **id** | **str**| Unique identifier expressed as UUID. | 

### Return type

[**Subscription**](Subscription.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

