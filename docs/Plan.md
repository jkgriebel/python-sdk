# Plan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Plan unique identifier as UUID. | [optional] 
**stripe_id** | **str** | Stripe identifier to identify entity charging for plans. | [optional] 
**created** | **str** | Date and time when plan was created. | [optional] 
**metadata** | **object** | Optional meta data object. | [optional] 
**livemode** | **bool** | Boolean to determine whether plan is live, or not. | [optional] 
**amount** | **int** | Amount (cost/price) of plan. | 
**currency** | **str** | Currency used to specify amount. | [optional] 
**interval** | **str** | Interval for plan, such as montly or yearly. | 
**interval_count** | **int** |  | 
**name** | **str** | Plan name. | 
**statement_descriptor** | **str** | Statement that describes plan to users. | [optional] 
**trial_period_days** | **int** | Number of days for plan trial, used for try and buy campaigns. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


