# InvoiceItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | InvoiceItem unique identifier expressed as UUID. | [optional] 
**stripe_id** | **str** | Stripe account identifier. | 
**created** | **str** | Date and time when invoice was created. | 
**metadata** | **object** | Optional metadata object of invoice. | [optional] 
**livemode** | **bool** | Boolean that determines whether invoice is live, or not. | [optional] 
**invoice** | **str** | Invoice unique identifier expressed as UUID. | 
**amount** | **int** | Amount the the invoice item will be billed for. | 
**currency** | **str** | Currency used in invoice. | 
**invoice_date** | **str** | Date the item was added to the invoice. | 
**proration** | **bool** | Whether or not the items cost will be prorated for the billing period. | [optional] 
**quantity** | **int** | Number of units for this item. | 
**description** | **str** | Item description. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


