# Card

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Card name. | [optional] 
**address_line1** | **str** | Address line 1. | [optional] 
**address_line2** | **str** | Address line 2. | [optional] 
**address_city** | **str** | Address city. | [optional] 
**address_state** | **str** | Address state. | [optional] 
**address_zip** | **str** | Address zip code. | [optional] 
**address_country** | **str** | Address country. | [optional] 
**exp_month** | **int** | Card expiration month. | [optional] 
**exp_year** | **int** | Card expiration year. | [optional] 
**token** | **str** | Card unique token. | [optional] 
**id** | **str** | Card unique identifier, expressed as UUID. | [optional] 
**customer** | **str** | Card customer. | [optional] 
**address_line1_check** | **str** | Address line 1 check, when processing card. | [optional] 
**address_zip_check** | **str** | Address check, when processing card. | [optional] 
**brand** | **str** | Credit or debit card brand. | [optional] 
**cvc_check** | **str** | Security code check. | [optional] 
**last4** | **str** | Last four digits of credit or debit card. | [optional] 
**fingerprint** | **str** | Card fingerprint. | [optional] 
**funding** | **str** | Funding. | [optional] 
**stripe_id** | **str** | Unique stripe identifier. | [optional] 
**created** | **str** | Card date and time creation. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


