# ServerStatistics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Server statistics unique identifier in UUID format. | [optional] 
**server** | **str** | Server name. | [optional] 
**start** | **str** | Start statistics, such as start time and date. | [optional] 
**stop** | **str** | Stop statistics, such as stop time and date. | [optional] 
**size** | **int** | Size of statistics. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


