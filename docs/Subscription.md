# Subscription

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Unique identifier for suscription as UUID. | [optional] 
**plan** | **str** | Plan name. | 
**stripe_id** | **str** | Stripe (payment processor) identifier. | [optional] 
**created** | **str** | Date and time suscription was created. | [optional] 
**livemode** | **bool** | Suscription live, or not. | [optional] 
**application_fee_percent** | **float** | Application fee percent. | [optional] 
**cancel_at_period_end** | **bool** | Boolean value to determine whether plan cancels at the end of the period, or not. | [optional] 
**canceled_at** | **str** | Date and time when plan was cancelled. | [optional] 
**current_period_start** | **str** | Current suscription plan start date. | [optional] 
**current_period_end** | **str** | Current suscription plan end date. | [optional] 
**start** | **str** | Date and time for when plan started. | [optional] 
**ended_at** | **str** | Date and time for when plan ended. | [optional] 
**quantity** | **int** | Quantity purchased as integer. | [optional] 
**status** | **str** | Suscription status. | [optional] 
**trial_start** | **str** | Date and time for trial start. | [optional] 
**trial_end** | **str** | Date and time for trial end. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


