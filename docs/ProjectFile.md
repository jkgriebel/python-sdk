# ProjectFile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | File unique identifier in UUID format. | [optional] 
**project** | **str** | Project name where file is located. | 
**content** | **str** | Data sent as string, in base64 format. | [optional] 
**name** | **str** | File name and extension. | [optional] 
**path** | **str** | File path. Defaults to root (/). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


