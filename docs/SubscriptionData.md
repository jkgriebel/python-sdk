# SubscriptionData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**plan** | **str** | Plan unique identifier. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


