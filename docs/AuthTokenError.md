# AuthTokenError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**non_field_errors** | **list[str]** | Errors not connected to any field. | [optional] 
**username** | **list[str]** | Username field errors. | [optional] 
**password** | **list[str]** | Password field errors. | [optional] 
**token** | **list[str]** | Token field errors. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


