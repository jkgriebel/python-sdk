# SubscriptionError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**non_field_errors** | **list[str]** | Errors not connected to any field | [optional] 
**id** | **list[str]** | id field errors | [optional] 
**plan** | **list[str]** | plan field errors | [optional] 
**stripe_id** | **list[str]** | stripe_id field errors | [optional] 
**created** | **list[str]** | created field errors | [optional] 
**livemode** | **list[str]** | livemode field errors | [optional] 
**application_fee_percent** | **list[str]** | application_fee_percent field errors | [optional] 
**cancel_at_period_end** | **list[str]** | cancel_at_period_end field errors | [optional] 
**canceled_at** | **list[str]** | canceled_at field errors | [optional] 
**current_period_start** | **list[str]** | current_period_start field errors | [optional] 
**current_period_end** | **list[str]** | current_period_end field errors | [optional] 
**start** | **list[str]** | start field errors | [optional] 
**ended_at** | **list[str]** | ended_at field errors | [optional] 
**quantity** | **list[str]** | quantity field errors | [optional] 
**status** | **list[str]** | status field errors | [optional] 
**trial_start** | **list[str]** | trial_start field errors | [optional] 
**trial_end** | **list[str]** | trial_end field errors | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


