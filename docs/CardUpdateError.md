# CardUpdateError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**non_field_errors** | **list[str]** | Errors not connected to any field | [optional] 
**name** | **list[str]** | name field errors | [optional] 
**address_line1** | **list[str]** | address_line1 field errors | [optional] 
**address_line2** | **list[str]** | address_line2 field errors | [optional] 
**address_city** | **list[str]** | address_city field errors | [optional] 
**address_state** | **list[str]** | address_state field errors | [optional] 
**address_zip** | **list[str]** | address_zip field errors | [optional] 
**address_country** | **list[str]** | address_country field errors | [optional] 
**exp_month** | **list[str]** | exp_month field errors | [optional] 
**exp_year** | **list[str]** | exp_year field errors | [optional] 
**token** | **list[str]** | token field errors | [optional] 
**id** | **list[str]** | id field errors | [optional] 
**customer** | **list[str]** | customer field errors | [optional] 
**address_line1_check** | **list[str]** | address_line1_check field errors | [optional] 
**address_zip_check** | **list[str]** | address_zip_check field errors | [optional] 
**brand** | **list[str]** | brand field errors | [optional] 
**cvc_check** | **list[str]** | cvc_check field errors | [optional] 
**last4** | **list[str]** | last4 field errors | [optional] 
**fingerprint** | **list[str]** | fingerprint field errors | [optional] 
**funding** | **list[str]** | funding field errors | [optional] 
**stripe_id** | **list[str]** | stripe_id field errors | [optional] 
**created** | **list[str]** | created field errors | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


