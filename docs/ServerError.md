# ServerError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**non_field_errors** | **list[str]** | Errors not connected to any field. | [optional] 
**id** | **list[str]** | id field errors. | [optional] 
**name** | **list[str]** | name field errors. | [optional] 
**created_at** | **list[str]** | created_at field errors. | [optional] 
**image_name** | **list[str]** | image_name field errors. | [optional] 
**server_size** | **list[str]** | server_size field errors. | [optional] 
**startup_script** | **list[str]** | startup_script field errors. | [optional] 
**config** | **list[str]** | config field errors. | [optional] 
**status** | **list[str]** | status field errors. | [optional] 
**connected** | **list[str]** | connected field errors. | [optional] 
**host** | **list[str]** | host field errors. | [optional] 
**endpoint** | **list[str]** | endpoint field errors. | [optional] 
**logs_url** | **list[str]** | logs_url field errors. | [optional] 
**status_url** | **list[str]** | status_url field errors. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


