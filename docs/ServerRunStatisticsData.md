# ServerRunStatisticsData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start** | **str** | Server start. | [optional] 
**stop** | **str** | Server stop. | [optional] 
**exit_code** | **int** | Server exit code. | [optional] 
**size** | **int** | Server size. | [optional] 
**stacktrace** | **str** | Server stacktrace. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


