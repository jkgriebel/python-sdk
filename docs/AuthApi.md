# swagger_client.AuthApi

All URIs are relative to *https://api.3blades.ai*

Method | HTTP request | Description
------------- | ------------- | -------------
[**auth_jwt_token_auth**](AuthApi.md#auth_jwt_token_auth) | **POST** /auth/jwt-token-auth/ | Create JSON Web Token (JWT)
[**auth_jwt_token_refresh**](AuthApi.md#auth_jwt_token_refresh) | **POST** /auth/jwt-token-refresh/ | Refresh a JSON Web Token (JWT)
[**auth_jwt_token_verify**](AuthApi.md#auth_jwt_token_verify) | **POST** /auth/jwt-token-verify/ | Validate JSON Web Token (JWT)
[**auth_register**](AuthApi.md#auth_register) | **POST** /auth/register/ | Register a user
[**oauth_login**](AuthApi.md#oauth_login) | **GET** /auth/login/{provider}/ | 


# **auth_jwt_token_auth**
> JWT auth_jwt_token_auth(jwt_data=jwt_data)

Create JSON Web Token (JWT)

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.AuthApi()
jwt_data = swagger_client.JWTData() # JWTData |  (optional)

try: 
    # Create JSON Web Token (JWT)
    api_response = api_instance.auth_jwt_token_auth(jwt_data=jwt_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthApi->auth_jwt_token_auth: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jwt_data** | [**JWTData**](JWTData.md)|  | [optional] 

### Return type

[**JWT**](JWT.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **auth_jwt_token_refresh**
> RefreshJSONWebToken auth_jwt_token_refresh(refreshjwt_data=refreshjwt_data)

Refresh a JSON Web Token (JWT)

Obtains a new JSON Web Token using existing user credentials.

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.AuthApi()
refreshjwt_data = swagger_client.RefreshJSONWebTokenData() # RefreshJSONWebTokenData |  (optional)

try: 
    # Refresh a JSON Web Token (JWT)
    api_response = api_instance.auth_jwt_token_refresh(refreshjwt_data=refreshjwt_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthApi->auth_jwt_token_refresh: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **refreshjwt_data** | [**RefreshJSONWebTokenData**](RefreshJSONWebTokenData.md)|  | [optional] 

### Return type

[**RefreshJSONWebToken**](RefreshJSONWebToken.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **auth_jwt_token_verify**
> VerifyJSONWebToken auth_jwt_token_verify(verifyjwt_data=verifyjwt_data)

Validate JSON Web Token (JWT)

Checks veraciy of token.

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.AuthApi()
verifyjwt_data = swagger_client.VerifyJSONWebTokenData() # VerifyJSONWebTokenData |  (optional)

try: 
    # Validate JSON Web Token (JWT)
    api_response = api_instance.auth_jwt_token_verify(verifyjwt_data=verifyjwt_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthApi->auth_jwt_token_verify: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **verifyjwt_data** | [**VerifyJSONWebTokenData**](VerifyJSONWebTokenData.md)|  | [optional] 

### Return type

[**VerifyJSONWebToken**](VerifyJSONWebToken.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **auth_register**
> User auth_register(user_data=user_data)

Register a user

User registration requires confirming email address to activate user.

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.AuthApi()
user_data = swagger_client.UserData() # UserData |  (optional)

try: 
    # Register a user
    api_response = api_instance.auth_register(user_data=user_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthApi->auth_register: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_data** | [**UserData**](UserData.md)|  | [optional] 

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **oauth_login**
> oauth_login(provider)



### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.AuthApi()
provider = 'provider_example' # str | OAuth2 provider

try: 
    api_instance.oauth_login(provider)
except ApiException as e:
    print("Exception when calling AuthApi->oauth_login: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **provider** | **str**| OAuth2 provider | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

