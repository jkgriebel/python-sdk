# ServerSizeError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**non_field_errors** | **list[str]** | Errors not connected to any field. | [optional] 
**id** | **list[str]** | Id field errors. | [optional] 
**name** | **list[str]** | Name field errors. | [optional] 
**cpu** | **list[str]** | CPU field errors. | [optional] 
**memory** | **list[str]** | Memory field errors. | [optional] 
**active** | **list[str]** | Active field errors. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


