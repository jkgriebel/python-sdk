# PlanData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metadata** | **object** | Plan meta data. | [optional] 
**livemode** | **bool** | Is plan live, or not. | [optional] 
**amount** | **int** | Plan amount in currency. | 
**currency** | **str** | Currency for plan. | [optional] 
**interval** | **list[str]** | Plan interval. | 
**interval_count** | **int** | Number of intervals. | 
**name** | **str** | Plan name. | 
**statement_descriptor** | **str** | Plan description. | [optional] 
**trial_period_days** | **int** | Trial days for try and buy campaigns. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


