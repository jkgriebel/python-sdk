# ServerSizeData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Server size name. | 
**cpu** | **int** | CPU set for server size. | 
**memory** | **int** | Memory set for server size. | 
**active** | **bool** | Active or not active. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


