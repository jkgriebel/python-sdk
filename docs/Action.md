# Action

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Action unique identifier, expressed as UUID. | [optional] 
**resource_uri** | **str** | Resource URI. | [optional] 
**payload** | **object** | Object action payload. | [optional] 
**action** | **str** | Action name. | 
**method** | **str** | Action method. | 
**user** | **str** | User that action is assigned to. | [optional] 
**user_agent** | **str** | User agent that action is related to. | 
**start_date** | **str** | Action date and time start. | [optional] 
**end_date** | **str** | Action needs to end before a certain date and time. | [optional] 
**state** | **str** | Action state. | 
**ip** | **str** | IP address that action is related to. | [optional] 
**object** | **str** | Action object. | [optional] 
**is_user_action** | **bool** | Is the action related to a user, or not. | [optional] 
**can_be_cancelled** | **bool** | Can action be cancelled, or not. | [optional] 
**can_be_retried** | **bool** | May action be retried, or not. | [optional] 
**path** | **str** | Action path. | [optional] 
**action_name** | **str** | Action name. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


