# DockerHost

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Unique identifier for docker host as UUID. | [optional] 
**name** | **str** | Docker host name. | 
**ip** | **str** | Docker host IPv4 address | 
**port** | **int** | Docker host port. | [optional] 
**status** | **str** | Docker host status. | [optional] 
**owner** | **str** | UUID of Docker host owner. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


