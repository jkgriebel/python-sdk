# SshTunnel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | SSH tunnel unique identifier in UUID format. | [optional] 
**server** | **str** | Server name. | [optional] 
**name** | **str** | SSH tunnel name. | 
**host** | **str** | Host, usually IPv4, for SSH tunnel. | 
**local_port** | **int** | Local source port for SSH tunnel. | 
**remote_port** | **int** | Remote port to establish SSH tunnel. | 
**endpoint** | **str** | SSH tunnel destination endpoint. | 
**username** | **str** | SSH tunnel user name. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


