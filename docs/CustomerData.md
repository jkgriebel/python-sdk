# CustomerData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metadata** | **object** | Customer meta data. | [optional] 
**account_balance** | **int** | Account balance. | [optional] 
**currency** | **str** | Customer preferred currency. | [optional] 
**last_invoice_sync** | **str** | List date and time invoice was synced. | [optional] 
**user** | **str** | User corresponding to customer. | 
**default_source** | **str** | Default source. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


