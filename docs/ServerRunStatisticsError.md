# ServerRunStatisticsError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**non_field_errors** | **list[str]** | Errors not connected to any field. | [optional] 
**id** | **list[str]** | id field errors. | [optional] 
**start** | **list[str]** | start field errors. | [optional] 
**stop** | **list[str]** | stop field errors. | [optional] 
**exit_code** | **list[str]** | exit_code field errors. | [optional] 
**size** | **list[str]** | size field errors. | [optional] 
**stacktrace** | **list[str]** | stacktrace field errors. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


