# UserProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**avatar** | **str** | Avatar image | [optional] 
**bio** | **str** | Description of user&#39;s biography. | [optional] 
**url** | **str** | Users&#39;s personal website. | [optional] 
**location** | **str** | User location. | [optional] 
**company** | **str** | User&#39;s company. | [optional] 
**timezone** | **str** | User&#39;s time zone. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


