# swagger_client.HostsApi

All URIs are relative to *https://api.3blades.ai*

Method | HTTP request | Description
------------- | ------------- | -------------
[**hosts_create**](HostsApi.md#hosts_create) | **POST** /v1/{namespace}/hosts/ | Create a new host
[**hosts_delete**](HostsApi.md#hosts_delete) | **DELETE** /v1/{namespace}/hosts/{host}/ | Delete a host
[**hosts_list**](HostsApi.md#hosts_list) | **GET** /v1/{namespace}/hosts/ | Get available hosts
[**hosts_read**](HostsApi.md#hosts_read) | **GET** /v1/{namespace}/hosts/{host}/ | Get a host
[**hosts_replace**](HostsApi.md#hosts_replace) | **PUT** /v1/{namespace}/hosts/{host}/ | Replace a host
[**hosts_update**](HostsApi.md#hosts_update) | **PATCH** /v1/{namespace}/hosts/{host}/ | Update a host


# **hosts_create**
> DockerHost hosts_create(namespace, dockerhost_data=dockerhost_data)

Create a new host

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.HostsApi()
namespace = 'namespace_example' # str | User or team name.
dockerhost_data = swagger_client.DockerHostData() # DockerHostData |  (optional)

try: 
    # Create a new host
    api_response = api_instance.hosts_create(namespace, dockerhost_data=dockerhost_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HostsApi->hosts_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **dockerhost_data** | [**DockerHostData**](DockerHostData.md)|  | [optional] 

### Return type

[**DockerHost**](DockerHost.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **hosts_delete**
> hosts_delete(namespace, host)

Delete a host

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.HostsApi()
namespace = 'namespace_example' # str | User or team name.
host = 'host_example' # str | DockerHost unique identifier expressed as UUID.

try: 
    # Delete a host
    api_instance.hosts_delete(namespace, host)
except ApiException as e:
    print("Exception when calling HostsApi->hosts_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **host** | **str**| DockerHost unique identifier expressed as UUID. | 

### Return type

void (empty response body)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **hosts_list**
> list[DockerHost] hosts_list(namespace, limit=limit, offset=offset, name=name, ordering=ordering)

Get available hosts

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.HostsApi()
namespace = 'namespace_example' # str | User or team data.
limit = 'limit_example' # str | Limite when getting items. (optional)
offset = 'offset_example' # str | Offset when getting items. (optional)
name = 'name_example' # str | Name, when getting items. (optional)
ordering = 'ordering_example' # str | Ordering when getting items. (optional)

try: 
    # Get available hosts
    api_response = api_instance.hosts_list(namespace, limit=limit, offset=offset, name=name, ordering=ordering)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HostsApi->hosts_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team data. | 
 **limit** | **str**| Limite when getting items. | [optional] 
 **offset** | **str**| Offset when getting items. | [optional] 
 **name** | **str**| Name, when getting items. | [optional] 
 **ordering** | **str**| Ordering when getting items. | [optional] 

### Return type

[**list[DockerHost]**](DockerHost.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **hosts_read**
> DockerHost hosts_read(namespace, host)

Get a host

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.HostsApi()
namespace = 'namespace_example' # str | User or team name.
host = 'host_example' # str | Unique identifier expressed as UUID or name.

try: 
    # Get a host
    api_response = api_instance.hosts_read(namespace, host)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HostsApi->hosts_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **host** | **str**| Unique identifier expressed as UUID or name. | 

### Return type

[**DockerHost**](DockerHost.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **hosts_replace**
> DockerHost hosts_replace(namespace, host, dockerhost_data=dockerhost_data)

Replace a host

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.HostsApi()
namespace = 'namespace_example' # str | User or team name.
host = 'host_example' # str | 
dockerhost_data = swagger_client.DockerHostData() # DockerHostData |  (optional)

try: 
    # Replace a host
    api_response = api_instance.hosts_replace(namespace, host, dockerhost_data=dockerhost_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HostsApi->hosts_replace: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **host** | **str**|  | 
 **dockerhost_data** | [**DockerHostData**](DockerHostData.md)|  | [optional] 

### Return type

[**DockerHost**](DockerHost.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **hosts_update**
> DockerHost hosts_update(namespace, host, dockerhost_data=dockerhost_data)

Update a host

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: jwt
swagger_client.configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# swagger_client.configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.HostsApi()
namespace = 'namespace_example' # str | User or team name.
host = 'host_example' # str | 
dockerhost_data = swagger_client.DockerHostData() # DockerHostData |  (optional)

try: 
    # Update a host
    api_response = api_instance.hosts_update(namespace, host, dockerhost_data=dockerhost_data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HostsApi->hosts_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**| User or team name. | 
 **host** | **str**|  | 
 **dockerhost_data** | [**DockerHostData**](DockerHostData.md)|  | [optional] 

### Return type

[**DockerHost**](DockerHost.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

