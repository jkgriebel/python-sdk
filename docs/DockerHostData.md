# DockerHostData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | DockerHost name. | 
**ip** | **str** | DockerHost IPv4 address. | 
**port** | **int** | DockerHost port. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


