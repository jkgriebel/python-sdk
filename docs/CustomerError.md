# CustomerError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**non_field_errors** | **list[str]** | Errors not connected to any field | [optional] 
**id** | **list[str]** | id field errors | [optional] 
**stripe_id** | **list[str]** | stripe_id field errors | [optional] 
**created** | **list[str]** | created field errors | [optional] 
**metadata** | **list[str]** | metadata field errors | [optional] 
**livemode** | **list[str]** | livemode field errors | [optional] 
**account_balance** | **list[str]** | account_balance field errors | [optional] 
**currency** | **list[str]** | currency field errors | [optional] 
**last_invoice_sync** | **list[str]** | last_invoice_sync field errors | [optional] 
**user** | **list[str]** | user field errors | [optional] 
**default_source** | **list[str]** | default_source field errors | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


