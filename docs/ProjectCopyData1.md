# ProjectCopyData1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**project** | **str** | UUID of the project the user wishes to copy. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


