# CollaboratorData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**owner** | **bool** | Project owner. Defaults to false. | [optional] 
**member** | **str** | Project member username. | 
**permissions** | **str** | Permissions assigned to collaborator. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


