# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | User unique identifier, expressed as UUID. | [optional] 
**username** | **str** | Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only. | 
**email** | **str** | User email address. | [optional] 
**first_name** | **str** | User first name. | [optional] 
**last_name** | **str** | User last name. | [optional] 
**profile** | [**UserProfile**](UserProfile.md) | User profile information. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


