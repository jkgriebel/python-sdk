# SshTunnelData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | SSH tunnel name. | 
**host** | **str** | SSH tunnel host. | 
**local_port** | **int** | SSH tunnel local port. | 
**remote_port** | **int** | SSH tunnel remote port. | 
**endpoint** | **str** | SSH tunnel endpoint. | 
**username** | **str** | User name to establish SSH tunnel. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


