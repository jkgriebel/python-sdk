# 3Blades/python-sdk Change Log
**Generated 20 December by [Nathaniel Compton](https://github.com/nathanielcompton)**

## Current Release: [v0.0.7](https://github.com/3blades/python-sdk/tree/HEAD)

[Full Changelog](https://github.com/3blades/python-sdk/compare/0.0.7...HEAD)

**Closed issues:**

- Add open source Docs [\#2](https://github.com/3Blades/python-sdk/issues/2)

**Merged pull requests:**

- OSS Docs [\#3](https://github.com/3Blades/python-sdk/pull/3) ([jgwerner](https://github.com/jgwerner))

## [0.0.7](https://github.com/3blades/python-sdk/tree/0.0.7) (2017-12-14)
[Full Changelog](https://github.com/3blades/python-sdk/compare/0.0.6...0.0.7)

## [0.0.6](https://github.com/3blades/python-sdk/tree/0.0.6) (2017-12-13)
[Full Changelog](https://github.com/3blades/python-sdk/compare/0.0.5...0.0.6)

## [0.0.5](https://github.com/3blades/python-sdk/tree/0.0.5) (2017-12-13)
[Full Changelog](https://github.com/3blades/python-sdk/compare/0.0.4...0.0.5)

## [0.0.4](https://github.com/3blades/python-sdk/tree/0.0.4) (2017-12-13)
