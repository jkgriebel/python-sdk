# coding: utf-8

"""
    3blades API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)

    OpenAPI spec version: 1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import os
import sys
import unittest

import swagger_client
from swagger_client.rest import ApiException
from swagger_client.apis.users_api import UsersApi


class TestUsersApi(unittest.TestCase):
    """ UsersApi unit test stubs """

    def setUp(self):
        self.api = swagger_client.apis.users_api.UsersApi()

    def tearDown(self):
        pass

    def test_me(self):
        """
        Test case for me

        A convenience endpoint that is equivalent to GET /v1/users/profiles/<my user id>/
        """
        pass

    def test_user_avatar_delete(self):
        """
        Test case for user_avatar_delete

        Delete avatar
        """
        pass

    def test_user_avatar_get(self):
        """
        Test case for user_avatar_get

        Retrieve user's avatar
        """
        pass

    def test_user_avatar_set(self):
        """
        Test case for user_avatar_set

        Add user avatar
        """
        pass

    def test_user_avatar_update(self):
        """
        Test case for user_avatar_update

        Update a project file
        """
        pass

    def test_users_api_key_list(self):
        """
        Test case for users_api_key_list

        Retrieve account's API key
        """
        pass

    def test_users_api_key_reset(self):
        """
        Test case for users_api_key_reset

        Reset a user's API key
        """
        pass

    def test_users_create(self):
        """
        Test case for users_create

        Create new user
        """
        pass

    def test_users_delete(self):
        """
        Test case for users_delete

        Delete a user
        """
        pass

    def test_users_emails_create(self):
        """
        Test case for users_emails_create

        Create an email address
        """
        pass

    def test_users_emails_delete(self):
        """
        Test case for users_emails_delete

        Delete an email address
        """
        pass

    def test_users_emails_list(self):
        """
        Test case for users_emails_list

        Retrieve account email addresses
        """
        pass

    def test_users_emails_read(self):
        """
        Test case for users_emails_read

        Retrieve a user's email addresses
        """
        pass

    def test_users_emails_replace(self):
        """
        Test case for users_emails_replace

        Replace an email address
        """
        pass

    def test_users_emails_update(self):
        """
        Test case for users_emails_update

        Update an email address
        """
        pass

    def test_users_list(self):
        """
        Test case for users_list

        Get user list
        """
        pass

    def test_users_read(self):
        """
        Test case for users_read

        Retrieve a user
        """
        pass

    def test_users_ssh_key_list(self):
        """
        Test case for users_ssh_key_list

        Retrieve an SSH key
        """
        pass

    def test_users_ssh_key_reset(self):
        """
        Test case for users_ssh_key_reset

        Recreate an SSH key
        """
        pass

    def test_users_update(self):
        """
        Test case for users_update

        Update a user
        """
        pass


if __name__ == '__main__':
    unittest.main()
