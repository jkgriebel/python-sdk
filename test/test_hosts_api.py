# coding: utf-8

"""
    3blades API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)

    OpenAPI spec version: 1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import os
import sys
import unittest

import swagger_client
from swagger_client.rest import ApiException
from swagger_client.apis.hosts_api import HostsApi


class TestHostsApi(unittest.TestCase):
    """ HostsApi unit test stubs """

    def setUp(self):
        self.api = swagger_client.apis.hosts_api.HostsApi()

    def tearDown(self):
        pass

    def test_hosts_create(self):
        """
        Test case for hosts_create

        Create a new host
        """
        pass

    def test_hosts_delete(self):
        """
        Test case for hosts_delete

        Delete a host
        """
        pass

    def test_hosts_list(self):
        """
        Test case for hosts_list

        Get available hosts
        """
        pass

    def test_hosts_read(self):
        """
        Test case for hosts_read

        Get a host
        """
        pass

    def test_hosts_replace(self):
        """
        Test case for hosts_replace

        Replace a host
        """
        pass

    def test_hosts_update(self):
        """
        Test case for hosts_update

        Update a host
        """
        pass


if __name__ == '__main__':
    unittest.main()
