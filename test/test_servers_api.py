# coding: utf-8

"""
    3blades API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)

    OpenAPI spec version: 1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import os
import sys
import unittest

import swagger_client
from swagger_client.rest import ApiException
from swagger_client.apis.servers_api import ServersApi


class TestServersApi(unittest.TestCase):
    """ ServersApi unit test stubs """

    def setUp(self):
        self.api = swagger_client.apis.servers_api.ServersApi()

    def tearDown(self):
        pass

    def test_servers_options_resources_read(self):
        """
        Test case for servers_options_resources_read

        Get a server size by id
        """
        pass

    def test_servers_options_server_size_create(self):
        """
        Test case for servers_options_server_size_create

        Create a new server size item
        """
        pass

    def test_servers_options_server_size_delete(self):
        """
        Test case for servers_options_server_size_delete

        Delete a server size by id
        """
        pass

    def test_servers_options_server_size_replace(self):
        """
        Test case for servers_options_server_size_replace

        Replace a server size by id
        """
        pass

    def test_servers_options_server_size_update(self):
        """
        Test case for servers_options_server_size_update

        Update a server size by id
        """
        pass

    def test_servers_options_sizes_list(self):
        """
        Test case for servers_options_sizes_list

        Retrieve available server sizes
        """
        pass


if __name__ == '__main__':
    unittest.main()
